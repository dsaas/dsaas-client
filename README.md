# For more information see:
[DSaaS Main Page](http://dsaas.pbit.co.za/)

[DSaaS Article Information](http://www.cs.sun.ac.za/~kroon/pubs/leroux2016dsaas/)

# Getting Started
## Install all the NodeJS dependencies
[sudo] npm install

## Building the JS folder
./scripts/build.sh