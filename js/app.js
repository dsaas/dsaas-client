var router = require('./router');
var React = require('react');

router.run((Handler, state) => {
	React.render(<Handler />, document.getElementById('app'));
});