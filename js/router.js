var React = require('react'),
	Router = require('react-router'),
	Route = Router.Route,
	DefaultRoute = Router.DefaultRoute,

	App = require('./views/App.react'),
	Home = require('./views/Home/Home.react'),
	SignUp = require('./views/Home/SignUp.react'),
	Access = require('./views/Access/Access.react'),
	JWT = require('./views/Access/JWT.react'),
	MapHandler = require('./views/Workbench/Map.react'),
	GraphHandler = require('./views/Workbench/GraphD3.react'),
	EditViewHandler = require('./views/Workbench/EditView.react'),
	Info = require('./views/Workbench/Info.react'),
	Shared = require('./views/Home/Shared.react'),
	JavaTut = require('./views/Home/JavaTut.react'),
	Workbench = require('./views/Workbench/Workbench.react');

var routes = (
	<Route name='app' path='/' handler={App}>
		<DefaultRoute name='home' handler={Home} />
		<Route name='signup' path='/signup/?' handler={SignUp} />
		<Route name='workbench' path='/workbench/?' handler={Workbench} />
		<Route name='map' path='/workbench/map/:namespace/:id/?:version?/?' handler={MapHandler}/>
		<Route name='graph' path='/workbench/graph/:namespace/:id/?:version?/?' handler={GraphHandler}/>
		<Route name='editMap' path='/workbench/edit/map/:namespace/:id/:version/?:key?' handler={EditViewHandler.Map}/>
		<Route name='editGraph' path='/workbench/edit/graph/:namespace/:id/:version/:objType/?:key?' handler={EditViewHandler.Graph}/>
		<Route name='access' path='/access/:namespace/:id/?' handler={Access}/>
		<Route name='info' path='/info/:namespace/:id/?' handler={Info}/>
		<Route name='shared' path='/shared/:namespace/?' handler={Shared}/>
		<Route name='jwt' path='/tokens/?' handler={JWT}/>
		<Route name='java-tutorial' path='/tutorial/java/?' handler={JavaTut}/>
	</Route>
);

module.exports = Router.create({
	routes: routes,
	location: Router.HistoryLocation
});

