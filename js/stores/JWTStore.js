var JWTActions = require('../actions/JWTActions'),
	StoreCreator = require('./StoreCreator');

var _store = {
	tokens: []
};

var JWTStore = StoreCreator({
	getTokens: () => { return _store.tokens; }
});

module.exports = JWTStore;

JWTActions.onEvents({
	TOKENS_RECEIVED: tokens => {
		_store.tokens = tokens;
		JWTStore.emitChange('Tokens received');
	},

	ADD_TOKEN: token => {
		_store.tokens.push(token);
		JWTStore.emitChange('Token added');
	},

	UPDATE_TOKEN: token => {
		_store.tokens = _store.tokens.map(t => {
			if (t.name == token.name) {
				token.token = t.token;
				return token;
			}
			return t;
		});
		JWTStore.emitChange('Token updated');
	},

	REMOVE_TOKEN: token => {
		for (var i = _store.tokens.length - 1; i >= 0; i--) {
			if (token.name == _store.tokens[i].name) break;
		}
		_store.tokens.splice(i, 1);
		JWTStore.emitChange('Token removed');
	}
});