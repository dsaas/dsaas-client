var SharedPageActions = require('../actions/SharedPageActions'),
	StoreCreator = require('./StoreCreator');

var _store = {
	datastructures: []
};

var SharedPageStore = StoreCreator({
	getDatastructures: function () {
		return _store.datastructures;
	}
});

module.exports = SharedPageStore;

SharedPageActions.onEvents({
	SHARED_DATASTRUCTURES_ARRIVED: (datastructures) => {
		_store.datastructures = datastructures;
		SharedPageStore.emitChange('Data Structures retrieved', 'success');
	}
});