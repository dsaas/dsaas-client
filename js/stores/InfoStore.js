var InfoActions = require('../actions/InfoActions'),
	utils = require('../utils'),
	StoreCreator = require('./StoreCreator');

var _store = {
	logs: []
};

var InfoStore = StoreCreator({
	getLogs: function () { return _store.logs; },
	getLastLogIndex: function () { return _store.lastIndex; },
	clear: function () { _store.logs = []; }
});

module.exports = InfoStore;

InfoActions.onEvents({
	NEW_LOGS: (entries, limit, ltDate) => {
		for (let e in entries) {
			let entry = entries[e];
			if (!entry.userId) {
				entry.userId = 'anonymous';
			}
			entry.date = utils.format(entry.date, 'D/M/Y h:m');
			_store.logs.push(entry);
		}
		InfoStore.emitChange('Logs Retrieved', 'success');
	}
});