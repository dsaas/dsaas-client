var WorkbenchActions = require('../actions/WorkbenchActions'),
	AuthActions = require('../actions/AuthActions'),
	AuthStore = require('./AuthStore'),
	StoreCreator = require('./StoreCreator');

function init() {
	return {
		datastructures: {}
	};
}

var _store = init();

var WorkbenchStore = StoreCreator({
	getDatastructures: () => { return _store.datastructures; },
	clear: function () { _store = init(); }
});

module.exports = WorkbenchStore;

WorkbenchActions.onEvents({
	UPDATE_DATASTRUCTURES: datastructures => {
		_store.datastructures = datastructures;
		console.log(_store.datastructures);
		WorkbenchStore.emitChange('Data structures received', 'success');
	},

	ADD_DATASTRUCTURE: (id, type, namespace) => {
		_store.datastructures[namespace || AuthStore.getUser().namespace].push({datastructureId: id, type: type, access: 'admin'});
		WorkbenchStore.emitChange('Data structure added', 'success');
	}
});

AuthActions.onEvents({
	SIGN_OUT: function () {
		WorkbenchStore.clear();
	}
});
