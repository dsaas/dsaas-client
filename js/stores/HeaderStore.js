var HeaderActions = require('../actions/HeaderActions'),
	StoreCreator = require('./StoreCreator');

var _store = {
	headerElements: []
};

var HeaderStore = StoreCreator({
	getHeaderElements: function () {
		return _store.headerElements;
	}
});

module.exports = HeaderStore;

HeaderActions.onEvents({
	ADD_HEADER_ELEMENTS: function (elementsToAdd) {
		// Note that elements to add should have a name and an action
		for (let elementName in elementsToAdd) {
			let element = elementsToAdd[elementName];
			if (!element.fn) throw new Error('Need a fn function.');

			_store.headerElements[elementName] = element;
		}
		HeaderStore.emitChange('Add Header Elements', '');
	},

	REMOVE_HEADER_ELEMENTS: function (elementsToRemove) {
		for (let elementName in elementsToRemove) {
			if (_store.headerElements[elementName]) {
				delete _store.headerElements[elementName];
			}
		}
		HeaderStore.emitChange('Remove Header Elements', '');
	}
});