var GraphActions = require('../actions/GraphActions'),
	AuthStore = require('./AuthStore'),
	StoreCreator = require('./StoreCreator');

var currentVersion = '0';
var _store = {
	graphs: {},
	data: {}
};

var GraphStore = StoreCreator({
	getVersion: function (version) {
		currentVersion = version;
		if (!_store[version]) {
			var user = AuthStore.getUser();
			GraphActions.fetchGraphEntries(_store.data.namespace, _store.data.id, version);
			GraphActions.fetchGraphEdges(_store.data.namespace, _store.data.id, version);
		} else {
			GraphStore.emitChange();
		}
	},
	getNodes: function () {
		if (_store.graphs[currentVersion])
			return _store.graphs[currentVersion].nodes;
		return null;
	},
	getEdges: function () {
		if (_store.graphs[currentVersion])
			return _store.graphs[currentVersion].edges;
		return null;
	},
	getGraph: function (version) {
		return _store.graphs[version];
	},
	setData: function (data) {
		_store.data = data;
	}
});

module.exports = GraphStore;

GraphActions.onEvents({
	NEW_GRAPH_VERSION: function (version, entries) {
		if (!_store.graphs[version])
			_store.graphs[version] = {nodes: entries};
		else
			_store.graphs[version].nodes = entries;
		GraphStore.emitChange('Graph retrieved', 'success');
	},

	ADDED_NODE: function (version, key, value) {
		if (!_store.graphs[version])
			_store.graphs[version] = {};
		
		if (!_store.graphs[version].nodes)
			_store.graphs[version].nodes = {};
		_store.graphs[version].nodes[key] = value;

		GraphStore.emitChange('Added a new node', 'success');
	},

	ADDED_EDGE: function (version, fromID, toID, value, body) {
		if (!_store.graphs[version])
			_store.graphs[version] = {};

		if (!_store.graphs[version].edges)
			_store.graphs[version].edges = {};

		_store.graphs[version].edges[body.id] = value;

		GraphStore.emitChange('Added a new edge', 'success');
	},

	NEW_GRAPH_EDGES: function (version, edges) {
		if (!_store.graphs[version])
			_store.graphs[version] = {edges: edges};
		else
			_store.graphs[version].edges = edges;
		GraphStore.emitChange('Graph Edges retrieved', 'success');
	},

	OBJ_FETCHED: function (type, version, key, value) {
		if (!_store.graphs[version])
			_store.graphs[version] = {nodes: {}, edges: {}};
		_store.graphs[version][type][key] = value;
		GraphStore.emitChange('New object fetched', 'success');
	},

	NODE_REMOVED: function (version, key) {
		if (!_store.graphs[version]) return;
		delete _store.graphs[version].nodes[key];
	},

	EDGE_REMOVED: function (version, key) {
		if (!_store.graphs[version]) return;
		delete _store.graphs[version].edges[key];
	}

});