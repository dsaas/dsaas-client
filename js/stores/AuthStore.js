var	AuthActions = require('../actions/AuthActions'),
	WorkbenchActions = require('../actions/WorkbenchActions'),
	storage = require('store'),
	StoreCreator = require('./StoreCreator');

var _authStore = storage.get('AuthStore') || {};
var AuthStore = StoreCreator({
	getUser:  () => { return _authStore.user  || {}; },
	getInfo:  () => { return _authStore.info  || ''; },
	getToken: () => { return _authStore.token || ''; }
});

module.exports = AuthStore;


function updateUser(user) {
	_authStore.user = _authStore.user || {};
	for (let k in user) _authStore.user[k] = user[k];
	AuthStore.emitChange();
}

function changed() {
	storage.set('AuthStore', _authStore);
}

WorkbenchActions.onEvents({
	UPDATE_USER: updateUser
}, changed)

AuthActions.onEvents({
	SIGN_IN_ERROR: action => { 
		AuthStore.emitChange('Sign In Failed: ' + action.error, 'error');
	},
	UPDATE_USER: updateUser,
	AUTH_TOKEN: token => {
		_authStore.token = token;
		AuthStore.emitChange('Sign In Success');
		if (!_authStore.user.namespace) {
			AuthActions.getCurrentUser();
		}
	},
	SIGN_OUT: () => {
		_authStore = {};
		storage.remove('AuthStore');
		AuthStore.emitChange('User signed out');
	}

}, changed);