var MapActions = require('../actions/MapActions'),
	AuthStore = require('./AuthStore'),
	StoreCreator = require('./StoreCreator');

var currentVersion = '0';
var _store = {
	maps: {},
	data: {}
};

var MapStore = StoreCreator({
	getVersion: version => {
		currentVersion = version;
		if (!_store[version]) {
			var user = AuthStore.getUser();
			MapActions.fetchVersionEntries(_store.data.namespace, _store.data.id, version);
		} else {
			MapStore.emitChange();
		}
	},
	getMap: (version) => {
		if (version) {
			return _store.maps[version];
		} else {
			return {map: _store.maps[currentVersion]};
		}
	},
	setData: (data) => {
		_store.data = data;
	}
});

module.exports = MapStore;

MapActions.onEvents({
	NEW_MAP_VERSION: (version, entries) => {
		_store.maps[version] = entries;
		MapStore.emitChange('Entries retrieved', 'success');
	},
	ADD_KEY_VALUE_PAIR: function (version, entry) {
		if (!_store.maps[version]) _store.maps[version] = {};
		_store.maps[version][entry.key] = entry.value;
		MapStore.emitChange('Added entry', 'success');
	},
	REMOVE_KEY_VALUE_PAIR: function (version, key) {
		if (_store.maps[version][key]) {
			delete _store.maps[version][key];
		}
		MapStore.emitChange('Removed entry', 'success');
	}
});