var EventEmitter = require('events').EventEmitter,
	extend = require('object-assign');

// Store Creator
module.exports = function (store) {
	var _store = extend({}, EventEmitter.prototype, store);

	// Sugar for the emiting and receiving of events
	// Note that the arguments are mostly for the toastr (See Partials/Toaster.react.js)
	_store.emitChange = function () {
		var args = ['change'];
		for (let a in arguments) args.push(arguments[a]);
		_store.emit.apply(this, args);
	};


	_store.removeChangeListener = func => {
		_store.removeListener('change', func);
	};

	_store.onChange = func => {
		_store.on('change', func);
	};

	return _store;
};