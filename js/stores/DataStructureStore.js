var DataStructureActions = require('../actions/DataStructureActions'),
	GraphActions = require('../actions/GraphActions'),
	StoreCreator = require('./StoreCreator');

var _store = {
	datastructure: {},
	versionHistory: {},
	lastVersionAdded: null
};

var DataStructureStore = StoreCreator({
	getDatastructure: version => { return _store.datastructure[version]; },
	getVersionHistory: () => { return _store.versionHistory; },
	lastVersionAdded: function () { return _store.lastVersionAdded; },
	remove: () => {  _store.versionHistory = {}; }
});

module.exports = DataStructureStore;

function addVersion(parent, child) {
	if (!_store.versionHistory[parent])
		_store.versionHistory[parent] = {children: [], parents: []};

	if (!_store.versionHistory[child])
		_store.versionHistory[child] = {children: [], parents: []};
	
	if (_store.versionHistory[parent].children.indexOf(child) == -1)
		_store.versionHistory[parent].children.push(child);
	if (_store.versionHistory[child].parents.indexOf(parent) == -1)
		_store.versionHistory[child].parents.push(parent);

	_store.lastVersionAdded = child;

	DataStructureStore.emitChange('Added new version');
}

DataStructureActions.onEvents({
	REFRESH_VERSION_HISTORY: function (history) {
		if (Object.keys(_store.versionHistory).length === 0) {
			_store.versionHistory = history;
		}
		DataStructureStore.emitChange('Version History Updated', 'success');
	},
	NEW_VERSION_ARRIVED: addVersion,
	SOCKET_REGISTERED: function (socket) {
		socket.on('versionAdded', function (child, parent) {
			addVersion(parent, child);
		});
	}
});

GraphActions.onEvents({
	ADDED_NODE: function (version, key, value, body) {
		addVersion(version, body.version);
	},

	ADDED_EDGE: function (version, fromID, toID, value, body) {
		addVersion(version, body.version);
	}
});