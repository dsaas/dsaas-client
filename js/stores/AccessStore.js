var AccessActions = require('../actions/AccessActions'),
	StoreCreator = require('./StoreCreator');

var _store = {
	permissions: [],
	userList:  {}
};

var AccessStore = StoreCreator({
	getUserPermissions: function () { return _store.permissions; },
	getSuggestedUserList: function () { return _store.suggestedUserList; }
});

module.exports = AccessStore;

AccessActions.onEvents({
	USER_PERMISSIONS: function (permissions) {
		_store.permissions = [];
		for (var permission in permissions) {
			permission = permissions[permission];
			if (permission._user) {
				_store.permissions.push({email: permission._user.email || permission._user, type: permission.type});
			}
		}
		AccessStore.emitChange('Permissions Retrieved', 'success');
	},
	USER_LIST_RECV: function (list) {
		_store.suggestedUserList = list;
		AccessStore.emitChange('User List Received', 'success');
	},
	UPDATE_USER: function (user) {
		for (var idx in _store.permissions) {
			if (_store.permissions[idx].email == user.email) {
				_store.permissions[idx].type = user.type;
				AccessStore.emitChange('User Updated', 'success');
				return;
			}
		}
		_store.permissions.push(user);
		AccessStore.emitChange('User Added', 'success');
	},
	REMOVE_USER_ACCESS: function (email) {
		for (var i = _store.permissions.length - 1; i >= 0; i--) {
			if (email == _store.permissions[i].email) break;
		}
		if (i != -1) {
			_store.permissions.splice(i, 1);
		}
		AccessStore.emitChange('User Removed', 'success');
	}
});