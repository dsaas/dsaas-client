var request = require('superagent');

function AuthRequest(method, url) {
	var AuthStore = require('../stores/AuthStore');
	var token = AuthStore.getToken();
	// if (!token) throw Error('Authentication token not available.');
	return request[method](url).set('Authorization', token);
};

module.exports = {
	AuthRequest: AuthRequest,
	request: request,
	
	// User API
	getNamespace: function (cb) {
		AuthRequest('get', '/api/user').end((err, res) => {
			if (err) throw err;
			cb(null, res.body.namespace);
		});
	},
	signIn: function (obj, cb) {
		request.post('/api/user/signin').send(obj).end(cb);
	},
	createUser: function (user, cb) {
		request.post('/api/user').send(user).end(cb);
	},
	getCurrentUser: function (cb) {
		AuthRequest('get', '/api/user').end(cb);
	},
	signOut: function (cb) {
		AuthRequest('post', '/api/user/signout').end(cb);
	},

	// Datastructures API
	putObject: function (namespace, id, version, key, value, cb) {
		AuthRequest('post', '/api/'+namespace+'/'+id+'/'+version+'/map/'+key).send(value).end(cb);
	},

	// Datastructure API
	getVersionHistory: function (namespace, id, cb) {
		AuthRequest('get', '/api/'+namespace+'/'+id + '/history').end(cb);
	},

	// Tokens
	getTokens: function (cb) {
		AuthRequest('get', '/api/token').end(cb);
	},
	createToken: function (name, cb) {
		AuthRequest('put', '/api/token/' + name).end(cb);
	},
	getToken: function (name, cb) {
		AuthRequest('get', '/api/token/' + name).end(cb);
	},
	removeToken: function (name, cb) {
		AuthRequest('del', '/api/token/' + name).end(cb);
	},
	changeStatus: function (name, status, cb) {
		if (status)
			AuthRequest('post', '/api/token/' + name + '/enable').end(cb);
		else
			AuthRequest('post', '/api/token/' + name + '/disable').end(cb);
	}
};