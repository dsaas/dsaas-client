var	ActionCreator = require('./ActionCreator'),
	WebUtils = require('../web-utils/index');

module.exports = ActionCreator({
	getTokens: function () {
		WebUtils.getTokens((err, res) => {
			if (err) throw err;
			this.emit('TOKENS_RECEIVED', res.body);
		});
	},

	createToken: function (name) {
		WebUtils.createToken(name, err => {
			if (err) throw err;
			WebUtils.getToken(name, (err, res) => {
				if (err) throw err;
				this.emit('ADD_TOKEN', {token: res.body.token, name: name, enabled: true});
			});
		});
	},

	changeStatus: function (name, enable) {
		WebUtils.changeStatus(name, enable, err => {
			if (err) throw err;
			this.emit('UPDATE_TOKEN', {name: name, enabled: enable});
		});
	},

	removeToken: function (name) {
		WebUtils.removeToken(name, err => {
			if (err) throw err;
			this.emit('REMOVE_TOKEN', {name: name});
		});
	}
});