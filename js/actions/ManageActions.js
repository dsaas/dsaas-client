var Reflux = require('reflux'),
	ManageConstants = require('../constants/ManageConstants');

module.exports = {
	fetchDatastructures: function (namespace) {
	
	},
	add: function () {

	},
	remove: function () {

	},
	updateNamespace: function (namespace) {
		localStorage.namespace = namespace;
		this.fetchDatastructures(namespace);
		AppDispatcher.dispatch({
			type: ManageConstants.SET_NAMESPACE,
			data: namespace
		});
	}	
};