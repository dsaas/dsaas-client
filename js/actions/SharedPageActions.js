var	ActionCreator = require('./ActionCreator'),
	WebUtils = require('../web-utils/index');

module.exports = ActionCreator({
	getSharedDatastructures: function (namespace) {
		WebUtils.request.get('/api/shared/' + namespace, (err, res) => {
			if (err) throw err;
			this.emit('SHARED_DATASTRUCTURES_ARRIVED', res.body);
		});
	}
});