var	ActionCreator = require('./ActionCreator'),
	WebUtils = require('../web-utils/index');

module.exports = ActionCreator({
	fetchGraphEntries: function (namespace, id, version) {
		WebUtils.AuthRequest('get', '/api/'+namespace+'/'+id+'/'+version+'/graph/nodes?full=true')
		.end((err, res) => {
			if (err) throw err;
			this.emit('NEW_GRAPH_VERSION', version, res.body);
		});
	},

	fetchGraphEdges: function (namespace, id, version) {
		WebUtils.AuthRequest('get', '/api/'+namespace+'/'+id+'/'+version+'/graph/edges?full=true')
		.end((err, res) => {
			if (err) throw err;
			this.emit('NEW_GRAPH_EDGES', version, res.body);
		});
	},

	addGraphNode: function (namespace, id, version, key, value) {
		WebUtils.AuthRequest('post', '/api/'+namespace+'/'+id+'/'+version+'/graph/node/'+key)
		.send(value)
		.end((err, res) => {
			if (err) throw err;
			this.emit('ADDED_NODE', version, key, value, res.body);
		});
	},

	addGraphEdge: function (namespace, id, version, fromValue, toValue, value) {
		WebUtils.AuthRequest('post', '/api/'+namespace+'/'+id+'/'+version+'/graph/edge/'+fromValue+'/'+toValue)
		.send(value)
		.end((err, res) => {
			if (err) throw err;
			this.emit('ADDED_EDGE', version, fromValue, toValue, value, res.body);
		});
	},

	fetch: function (namespace, id, version, key, objType) {
		WebUtils.AuthRequest('get', '/api/'+namespace+'/'+id+'/'+version+'/graph/'+objType.substring(0,objType.length-1)+'/'+key)
		.end((err, res) => {
			if (err) throw err;
			this.emit('OBJ_FETCHED', objType, version, key, res.body);
		});
	},

	remove: function (namespace, id, version, key, objType) {
		if (objType == 'nodes') {
			WebUtils.AuthRequest('del', '/api/'+namespace+'/'+id+'/'+version+'/graph/node/'+key)
			.end((err, res) => {
				if (err) throw err;
				this.emit('NODE_REMOVED', version, key);
			});

		} else {
			WebUtils.AuthRequest('del', '/api/'+namespace+'/'+id+'/'+version+'/graph/edge/'+key)
			.end((err, res) => {
				if (err) throw err;
				this.emit('EDGE_REMOVED', version, key);
			});
		}
	}

});