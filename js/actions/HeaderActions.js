var	ActionCreator = require('./ActionCreator');

module.exports = ActionCreator({
	addHeaderElements: function (elementsToAdd) {
		this.emit('ADD_HEADER_ELEMENTS', elementsToAdd);
	},

	removeHeaderElements: function (elementsToRemove) {
		this.emit('REMOVE_HEADER_ELEMENTS', elementsToRemove);
	}
});