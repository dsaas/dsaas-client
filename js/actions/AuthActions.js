'use strict';

var	ActionCreator = require('./ActionCreator'),
	WebUtils = require('../web-utils/index');

module.exports = ActionCreator({
	customSignIn: function (email, password) {
	
	},

	signIn: function (obj, user) {

		var router = require('../router');

		this.emit('UPDATE_USER', user);
		WebUtils.signIn(obj, (err, res) => {
			if (err) {
				if (err.status == 401) {
					router.transitionTo('/signup/');
					err.message = 'User does not exist, please create a new user.';
				}
				throw err;
			} else {
				res.password = '';
				this.emit('AUTH_TOKEN', res.text);
				router.transitionTo('/workbench/');
			}
		});
	},

	createUser: function (user) {
		
		var router = require('../router');

		WebUtils.createUser(user, err => {
			if (err) throw err;

			this.emit('UPDATE_USER', user);

			WebUtils.signIn(user, (err, res) => {
				if (err) throw err;

				this.emit('AUTH_TOKEN', res.text);
				router.transitionTo('/workbench/');
			});
		});
	},

	getCurrentUser: function () {
		WebUtils.getCurrentUser((err, res) => {
			if (err) throw err;
			this.emit('UPDATE_USER', res.body);
		});
	},

	signOut: function () {
		var router = require('../router');
		WebUtils.signOut(err => {
			if (err) {
				if (err.message == 'Unauthorized') {}
				else throw err;
			}
			this.emit('SIGN_OUT');
			router.transitionTo('/');
		});
	}
});