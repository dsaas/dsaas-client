var EventEmitter = require('events').EventEmitter,
	extend = require('object-assign');

var _actionCreator = extend({}, EventEmitter.prototype);

// Add the on-events function
_actionCreator.onEvents = (eventListeners, afterEachListener) => {
	// Bind all the eventListeners to events
	for (let listener in eventListeners) {
		((eventName, func) => {
			_actionCreator.on(eventName, function () {
				func.apply(null, arguments);
				if (afterEachListener) afterEachListener();
			});
		})(listener, eventListeners[listener]);
	}
};

var ActionCreator = actions => {
	// Bind the actions to the action creator
	for (let action in actions) {
		_actionCreator[action] = function () {
			actions[action].apply(_actionCreator, arguments)
		};
	}
	return _actionCreator;
};

module.exports = ActionCreator;