var	ActionCreator = require('./ActionCreator'),
	WebUtils = require('../web-utils/index');

module.exports = ActionCreator({
	getLogs: function (namespace, id, limit, ltDate) {
		var url = '/api/' + namespace +'/' + id + '/log?q=-_id -type -namespace';

		if (limit) url += 'limit=' + limit + '&';
		if (ltDate) url += 'date=' + ltDate + '&';

		WebUtils.AuthRequest('get', url).end((err, res) => {
			if (err) throw err;
			this.emit('NEW_LOGS', res.body, limit, ltDate);
		});
	}
});