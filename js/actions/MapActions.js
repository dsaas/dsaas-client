var	ActionCreator = require('./ActionCreator'),
	WebUtils = require('../web-utils/index');

module.exports = ActionCreator({
	fetchVersionEntries: function (namespace, id, version) {
		WebUtils.AuthRequest('get', '/api/'+namespace+'/'+id+'/'+version+'/map/entries?start=0&end=100')
			.end((err, res) => {
				if (err) throw err;
				this.emit('NEW_MAP_VERSION', version, res.body);
		});
	},

	fetchSingleEntry: function (namespace, id, version, key) {
		WebUtils.AuthRequest('get', '/api/' + namespace + '/' + id + '/' + version + '/map/' + key)
			.end((err, res) => {
				if (err) throw err;
				var entry = {
					key: key,
					value: res.body
				};
				this.emit('ADD_KEY_VALUE_PAIR', version, entry);
			});
	},

	removeObject: function (namespace, id, version, key) {
		WebUtils.AuthRequest('del', '/api/' + namespace + '/' + id + '/' + version + '/map/' + key)
			.end((err, res) => {
				if (err) throw err;
				this.emit('REMOVE_KEY_VALUE_PAIR', version, key);
			});
	}
});