var	ActionCreator = require('./ActionCreator'),
	WebUtils = require('../web-utils/index');

module.exports = ActionCreator({
	populateWorkbenchStore: function () {
		WebUtils.AuthRequest('get', '/api/datastructures')
		.end((err, res) => {
			if (err) throw err;
			this.emit('UPDATE_DATASTRUCTURES', res.body);
		});
	},

	createDatastructure: function (namespace, id, type) {
		WebUtils.AuthRequest('post', '/api/'+namespace+'/'+id).send({type: type})
			.end((err, res) => {
				if (err) throw err;
				this.emit('ADD_DATASTRUCTURE', id, type);
			});
	},

	importDatastructure: function (namespace, id, type, file) {
		var reader = new FileReader();
		var self = this;
		reader.onload = function (e) {
			WebUtils.AuthRequest('post', '/api/'+namespace+'/import')
				.send({
					datastructureId: id,
					type: type,
					data: e.target.result
				})
				.end(function (err, res) {
					if (err) throw err;
					self.emit('ADD_DATASTRUCTURE', id, type);
				});
		};
		reader.readAsText(file);
	},

	fork: function (originalNamespace, originalId, newNamespace, newId) {
		WebUtils.AuthRequest('post', '/api/' + originalNamespace + '/' + originalId + '/fork')
			.send({namespace: newNamespace, datastructureId: newId})
			.end((err, res) => {
				if (err) throw err;
				this.emit('ADD_DATASTRUCTURE', newId, res.body.type, newNamespace);
			});
	}
});