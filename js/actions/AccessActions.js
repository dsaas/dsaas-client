var	ActionCreator = require('./ActionCreator'),
	WebUtils = require('../web-utils/index');

module.exports = ActionCreator({
	getUserPermissions: function (namespace, id) {
		WebUtils.AuthRequest('get', '/api/' + namespace + '/' + id + '/access')
		.end((err, res) => {
			this.emit('USER_PERMISSIONS', res.body);
		});
	},

	getUsersStartingWith: function (word) {
		WebUtils.request.get('/api/users/?email=' + word)
		.end((err, res) => {
			if (err) throw err;
			this.emit('USER_LIST_RECV', res.body);
		});
	},

	addUser: function (namespace, id, value) {
		WebUtils.AuthRequest('put', '/api/' + namespace + '/' + id + '/access/' + value)
			.send({type: 'read'})
			.end((err, res) => {
				if (err) throw err;
				this.emit('UPDATE_USER', {email: value, type: 'read'});
			});
	},

	changeUser: function (namespace, id, email, type) {
		WebUtils.AuthRequest('post', '/api/' + namespace + '/' + id + '/access/' + email)
			.send({type: type})
			.end((err, res) => {
				if (err) throw err;
				this.emit('UPDATE_USER', {email: email, type: type});
			});
	},

	removeUser: function (namespace, id, email) {
		WebUtils.AuthRequest('del', '/api/' + namespace + '/' + id +  '/access/' + email)
			.end((err, res) => {
				if (err) throw err;
				this.emit('REMOVE_USER_ACCESS', email);
			});
	}
});