var	ActionCreator = require('./ActionCreator'),
	io = require('socket.io-client'),
	WebUtils = require('../web-utils/index');

var socket;

module.exports = ActionCreator({
	getVersionHistory: function (namespace, id) {
		WebUtils.getVersionHistory(namespace, id, (err, res) => {
			if (err) throw err;
			this.emit('REFRESH_VERSION_HISTORY', res.body);
		});
	},
	putObject: function (namespace, id, prevVersion, key, value) {
		WebUtils.putObject(namespace, id, prevVersion, key, value, (err, res) => {
			if (err) throw err;
			this.emit('NEW_VERSION_ARRIVED', prevVersion, res.body.version);
		});
	},
	merge: function (namespace, id, versionA, versionB) {
		WebUtils.AuthRequest('put', '/api/'+namespace+'/'+id+'/merge')
			.send({
				versionA: versionA,
				versionB: versionB
			}).end((err, res) => {
				if (err) throw err;
				this.emit('MERGED', versionA, versionB, res.body.version);
			});
	},
	registerSocket: function (namespace, id) {
		if (!socket)
			socket = io('http://'+window.location.hostname+':3000');
		else
			socket.connect();

		socket.emit('/api/history/register', namespace, id, function (err) {
			if (err) throw err;
		});
		this.emit('SOCKET_REGISTERED', socket);
	},
	unregisterSocket: function () {
		socket.emit('/api/history/unregister');
		socket.disconnect();
	},
	getSocket: function () { return socket; }
});