var keyMirror = require('keymirror');

module.exports = keyMirror({
	FETCH_DATA_STRUCTURES: null,
	SET_NAMESPACE: null
});