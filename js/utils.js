var React = require('react');

module.exports = {

	trunc: function (str, start, stop) {
		if (str) {
			return str.substring(0, start) + '..' + str.substring(str.length - stop);
		}
		return str;
	},

	wrap: function (Component, props) {
		return React.createClass({
			render: function() {
				props.params = this.props.params;
				return React.createElement(Component, props);
			}
		})
	},

	isArray: function (obj) {
		return Object.prototype.toString.call( obj ) === '[object Array]';
	},

	format: function (date, format) {
		date = new Date(date);
		let str = format.replace('D', date.getDate());
		str = str.replace('M', date.getMonth());
		str = str.replace('Y', date.getFullYear());
		str = str.replace('h', date.getHours());
		str = str.replace('m', date.getMinutes());
		return str;
	}
}