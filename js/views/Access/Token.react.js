var React = require('react'),
	JWTActions = require('../../actions/JWTActions'),
	ReactZeroClipboard = require('react-zeroclipboard');

module.exports = React.createClass({
	getInitialState: function () {
		return {};
	},

	render: function() {
		return (
			<div className='token'> 
				<div className='token-name inline'>{this.props.token.name}</div>
				<div className='token-button-group inline'>
					<ReactZeroClipboard text={this.props.token.token} onAfterCopy={() => { window.alert('Copied!'); }}>
						<button>Copy Token</button>
					</ReactZeroClipboard>
					<div className='token-enabled inline'>
						<button className={this.props.token.enabled ? 'selected' : ''} onClick={this._enable}>Enable</button>
						<button className={!this.props.token.enabled ? 'selected' : ''} onClick={this._disable}>Disable</button>
					</div>
					<div style={{'display': this.props.token.name == 'personal' ? 'none' : 'inline-block'}} className='inline'>
						<button onClick={e => { this.props.remove(this.props.token.name); }}>Remove Token</button>
					</div>
				</div>
			</div>
		);
	},

	_enable: function () {
		JWTActions.changeStatus(this.props.token.name, true);
	},

	_disable: function () {
		JWTActions.changeStatus(this.props.token.name, false);
	}

});