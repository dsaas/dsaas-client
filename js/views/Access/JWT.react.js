var React = require('react'),
	Router = require('react-router'),
	AuthStore = require('../../stores/AuthStore'),
	JWTActions = require('../../actions/JWTActions'),
	JWTStore = require('../../stores/JWTStore'),

	Token = require('./Token.react');


var newTokenName = '';
module.exports = React.createClass({
	mixins: [ Router.State ],

	getInitialState: function () {
		return {
			namespace: AuthStore.getUser().namespace,
			tokens: []
		};
	},

	componentWillMount: function () {
		JWTActions.getTokens();
		JWTStore.onChange(this._onChange);
	},

	componentWillUnmount: function () { JWTStore.removeChangeListener(this._onChange); },

	render: function () {
		return (
			<div className='tokens'>
				<h1>Token Management ({this.state.namespace})</h1>
				<h2>Creates JWT tokens that is used to authenticate you.
					Every token you create is bound to you and you have the ability to enable, disable or remove the authoritive properties of said token. <br></br>
					Use for authenticating scripts and external systems.</h2>
				<input type='text' onChange={e => {newTokenName = e.target.value;}}/>
				<button onClick={this._addToken}>Create</button>
				{this._showAllTokens()}
			</div>
		);
	},

	_showAllTokens: function () {
		return this.state.tokens.map(token => {
			return (
				<Token key={token.token} token={token} remove={ JWTActions.removeToken } />
			);
		});
	},

	_onChange: function () {
		this.setState({ tokens: JWTStore.getTokens() });
	},

	_addToken: function () { JWTActions.createToken(newTokenName); },

	statics: {
		willTransitionTo: function (transition, params) {
			if (!AuthStore.getToken()) transition.redirect('/');
		}
	}
});