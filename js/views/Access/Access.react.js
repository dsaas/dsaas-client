var React = require('react'),
	Router = require('react-router'),
	AutoComplete = require('../Partials/AutoComplete.react'),
	AccessStore = require('../../stores/AccessStore'),
	AuthStore = require('../../stores/AuthStore'),
	AccessActions = require('../../actions/AccessActions');

var addUserEmail;

module.exports = React.createClass({
	mixins: [ Router.State ],

	getInitialState: function () {
		return {
			userPermissions: [],
			userList: []
		};
	},

	componentWillMount: function () { 
		AccessStore.onChange(this._onChange);
		AccessActions.getUserPermissions(this.getParams().namespace, this.getParams().id);
	},

	componentWillUnmount: function () { 
		AccessStore.removeChangeListener(this._onChange);
	},

	render: function () {
				// <input type='text' placeholder='email'/>
		return (
			<div className='access'>
				<h1>Access Permissions of "{this.getParams().id}"</h1>
				<AutoComplete notify={this._autoComplete} placeholder='email' 
					list={this.state.suggestedUserList} manipulate={e => {return {primary: e.email, extra: e.name}}} 
					onChange={value => {addUserEmail = value}}/>
				<button onClick={this._addUser}>Add +</button>
				<div className='user-permissions'>
					{this._showUserPermissions()}
				</div>
			</div>
		);
	},

	_autoComplete: function (word) {
		if (word.length >= 3) {
			AccessActions.getUsersStartingWith(word);
		} else {
			this.setState({suggestedUserList: []});
		}
	},

	_showUserPermissions: function () {
		if (this.state.userPermissions.length > 0) {
			return this.state.userPermissions.map(user => {
				
				var isSpecialUser = user.email === 'public' || user.email ==='registered';
				var removeButtonStyle = {'display': isSpecialUser ? 'none' : 'inline'};

				return (
					<div className='user-permission'>
						<div className='user-email'>{user.email}</div>
						<div className='user-access-permission'>
							{this._getButtonSet(user.email, user.type)}
							<button className='remove' data-email={user.email} onClick={this._removePermission} style={removeButtonStyle}>
								Remove
							</button>
						</div>
					</div>
				);
			});
		} else {
			return (<div className='error'>No permissions has been set.</div>);
		}
	},

	_getButtonSet: function (email, access) {
		var isSpecialUser = email === 'public' || email === 'registered';
		var els = {'read': 'Read', 'read/write': 'Read/Write'};

		if (!isSpecialUser) {
			 els['admin'] = 'Admin';
		}

		if (email === 'public' || email === 'registered') {
			els['none'] = 'None';
		}
		
		var buttonList = [];
		for (var type in els) {
			buttonList.push((<button value={type} className={access == type ? 'selected': ''} data-email={email} onClick={this._changeUser}>
					{els[type]}
				</button>));
		}
		return buttonList;
	},

	_addUser: function (e) {
		if (addUserEmail) {
			var params = this.getParams();
			AccessActions.addUser(params.namespace, params.id, addUserEmail);
		}
	},

	_changeUser: function (e) {
		var type = e.target.value,
			email = e.target.getAttribute('data-email');

		var params = this.getParams();
		AccessActions.changeUser(params.namespace, params.id, email, type);
	},

	_onChange: function () {
		this.setState({
			userPermissions: AccessStore.getUserPermissions(),
			suggestedUserList: AccessStore.getSuggestedUserList()
		});
	},

	_removePermission: function (e) {
		var email = e.target.getAttribute('data-email');
		var params = this.getParams();
		AccessActions.removeUser(params.namespace, params.id, email);
	},

	statics: {
		willTransitionTo: function (transition, params) {
			if (!AuthStore.getToken()) transition.redirect('/');
		}
	}
});