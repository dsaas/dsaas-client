var React = require('react'),
	RouteHandler = require('react-router').RouteHandler,
	Highlight = require('react-highlight');

module.exports = React.createClass({
	render: function () {
		return (
			<div className='java-tut'>
				<div className='text'>
					The Java language binding is used to connect to your cloud data structures.
					<br></br>
					In this tutorial we will be using the graph data structure to build a network of users.
					Every node will be of the User class and every edge will be a String.
					<br></br>
					First register or sign in to the website.
					Create a data structure and either make sure that the data structure is publicly accessible or create a new access token.
					If you went the access token route, then save it in a file called "auth.txt" inside your java directory.
					<br></br>
					Next download the <a href='/library-binding/dsaas.jar'>JAR file</a>, include it in your compilation path and import the following classes.
				</div>
				<Highlight className='javascript'>
				  {"import za.co.pbit.dsaas.VersionControl; \nimport za.co.pbit.dsaas.CloudStructureFactory; \nimport za.co.pbit.dsaas.GraphCloudStructure;"}
				</Highlight>
				<div className='text'>
					You can store any Java class (<a href='https://sites.google.com/site/gson/gson-user-guide'>with some limitations</a>) in these data structures.
					We are going to create the following User class.
				</div>
				<Highlight className='javascript'>
					{"class User {\n\tprivate String name, surname;\n\tprivate int age;\n\tpublic User () {}"
					+"\n\n\tpublic User (String name, String surname, int age) {\n\t\tthis.name = name;\n\t\tthis.surname = surname;"
					+"\n\t\tthis.age = age;\n\t}\n\n\t@Override\n\tpublic String toString() {\n\t\t"
					+"return \"User: \" + name + \" \" + surname + \" \"  + age;\n\t}\n}"}
				</Highlight>
				<div className='text'>
					Next we need to get reference to the cloud datastructures. <br></br>
					First define variables for your namespace and the data structure identifier (given when the data structure was created on the server).
					We first create a factory instance which is passed the class variables.
				</div>
				<Highlight className='javascript'>
					{"String namespace = \"<your-namespace>\";\nString id = \"<your-data-structure-id>\";\n" +
					"CloudStructureFactory<GraphCloudStructure> graphFactory = new CloudStructureFactory<GraphCloudStructure>( \n namespace, id, GraphCloudStructure.class, User.class, String.class);"}
				</Highlight>
				<div className='text'>
					We then retrieve a version control instance.
				</div>
				<Highlight className='javascript'>
					{
						"VersionControl<GraphCloudStructure> graphVersionControl = graphFactory.getVersionControl();"}
				</Highlight>
				<div className='text'>
					And we can get our initial version!
				</div>
				<Highlight className='javascript'>
					{
						"GraphCloudStructure<User, String> graph = graphVersionControl.initialVersion();"}
				</Highlight>
				<div className='text'>
					And then used the API to build our graph.
				</div>
				<Highlight className='javascript'>
					{"graph.addVertex(\"node1\", new User(\"Peter\", \"Pan\", 11));\n"+
					 "graph.addVertex(\"node2\", new User(\"Alice\", \"A\", 30));\n"+
					 "graph.addVertex(\"node3\", new User(\"Ben\", \"Little\", 32));\n"+
					 "graph.addEdge(\"node1\", \"node2\", \"node2Connector\");\n"+
					 "graph.addEdge(\"node1\", \"node3\", \"node3Connector\");\n"
					}
				</Highlight>
				<div className='text'>
					The map cloud data structure has all the normal java.utils.Map methods, where as the graph cloud data structure has the following methods.
				</div>
				<Highlight className='javascript'>
					{"graph.addVertex(id);\ngraph.getVertex(id);\ngraph.getEdgesOfVertex(id);\ngraph.updateVertex(id,vertex);\n"
					+"graph.removeVertex(id);\ngraph.adjacent(id);\ngraph.parents(id);\ngraph.children(id);\ngraph.addEdge(id);\n"
					+"graph.updateEdge(id);\ngraph.getEdge(id);\ngraph.getVertices();\ngraph.getEdges();\ngraph.getVersion();"
					}
				</Highlight>
				For full documentation see: <a href='/documents/requirements.pdf' target='_blank'>Requirements Document</a>, <a href='/documents/design.pdf'  target='_blank'>Design Document</a>
			</div>
		);
	}
});


		// VersionControl<GraphCloudStructure> graphVC = graphFact.getVersionControl();
		// GraphCloudStructure<User, String> graph = graphVC.initialVersion();

		// graph.addVertex("node1", new User("Peter", "Pan", 11));
		// graph.addVertex("node2", new User("Alice", "A", 30));
		// graph.addVertex("node3", new User("Ben", "Little", 32));
		// graph.addEdge("node1", "node2", "node2Connector");
		// graph.addEdge("node1", "node3", "node3Connector");

		// for (String edge : graph.getEdgesOfVertex("node1")) {
		// 	System.out.println(edge);
		// }

