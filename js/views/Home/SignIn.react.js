var React = require('react'),
	AuthStore = require('../../stores/AuthStore'),
	AuthActions = require('../../actions/AuthActions'),
	Navigation = require('react-router').Navigation;

var email, password;
var setAuth;
var googleButton = null;

module.exports = React.createClass({

	mixins: [Navigation],

	componentWillMount: function () {
		window['onSignIn'] = this._googleSignIn;
		googleButton = (<div className='g-signin2' data-onsuccess='onSignIn' data-theme='dark'></div>);

	},

	componentDidMount: function () {
		setAuth = () => {
			this.setState({
				info: AuthStore.getInfo()
			});
		};

		AuthStore.on('change', setAuth);
	},
	
	componentWillUnmount: function (e) {
		AuthStore.removeListener('change', setAuth);
		delete window['onSignIn'];
		googleButton = null;
	},

	getInitialState: () => {
		return {info: 'INFO'};
	},

	_googleSignIn: function (googleUser) {
		var idToken = googleUser.getAuthResponse().id_token;
		var profile = googleUser.getBasicProfile();
		AuthActions.signIn({googleToken: idToken}, {name: profile.getName(), email: profile.getEmail()});
	},

	_githubSignIn: function (e) {
		alert('GitHub Sign In');
	},

	_signIn: function (e) {
		if (!email) throw Error('Please add the user\'s email and password');
		var user = {email: email, password: password};
		AuthActions.signIn(user, user);
	},

	render: function() {
					// <div className='email'>
					// 	<input type='text' name='email' placeholder='Email' onChange={e => { email = e.target.value; }} />
					// </div>
					// <div className='password'>
					// 	<input type='password' name='password' placeholder='Password' onChange={e => { password = e.target.value; }} />
					// </div>
					// <button onClick={this._signIn}>Enter</button>
		return (
			<div className='sign-in'>
				<div className='custom-sign-in'>
					{googleButton}
				</div>
			</div>
		);
					// <button onClick={e => {this.transitionTo('/passwordreset')}}>Password Reset</button>
	}
});