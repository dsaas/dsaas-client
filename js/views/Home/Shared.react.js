var React = require('react'),
	DatastructureOverviewContainer = require('../Workbench/DatastructureOverviewContainer.react'),
	Navigation = require('react-router').Navigation,
	SharedPageStore = require('../../stores/SharedPageStore'),
	SharedPageActions = require('../../actions/SharedPageActions');

module.exports = React.createClass({

	mixins: [Navigation],

	getInitialState: function () {
		return {
			datastructures: []
		};
	},

	componentWillMount: function () {
		SharedPageStore.onChange(this._onChange);
		SharedPageActions.getSharedDatastructures(this.props.params.namespace);
		this.setState({datastructures: SharedPageStore.getDatastructures()});
	},

	componentWillUnmount: function () {
		SharedPageStore.removeChangeListener(this._onChange);
	},

	render: function () {
		return (
			<div className='datastructures'>
				<h1>Shared Datastructures in "{this.props.params.namespace}"</h1>
				{this._renderDataStructures()}
			</div>
		);
	},

	_renderDataStructures: function () {
		var ds = [];
		for (var datastructure in this.state.datastructures) {
			datastructure = this.state.datastructures[datastructure];
			ds.push(<DatastructureOverviewContainer datastructure={datastructure} namespace={datastructure.namespace} sharedview={true}/>)
		}
		return ds;
	},

	_onChange: function () {
		this.setState({datastructures: SharedPageStore.getDatastructures()});
	}
});