var React = require('react'),
	RouteHandler = require('react-router').RouteHandler,
	AutoComplete = require('../Partials/AutoComplete.react'),
	DatastructureOverviewContainer = require('../Workbench/DatastructureOverviewContainer.react'),
	SharedPageStore = require('../../stores/SharedPageStore'),
	SharedPageActions = require('../../actions/SharedPageActions'),
	Navigation = require('react-router').Navigation,
	SignIn = require('./SignIn.react');

module.exports = React.createClass({
	mixins: [Navigation],

	getInitialState: function () {
		return {
			userList: [],
			users: [],
			datastructures: []
		}
	},

	componentWillMount: function () {
		SharedPageStore.onChange(this._onChange);
		SharedPageActions.getSharedDatastructures('');
		this.setState({datastructures: SharedPageStore.getDatastructures()});
	},

	componentWillUnmount: function () {
		SharedPageStore.removeChangeListener(this._onChange);
	},

	render: function () {
					// <AutoComplete list={this.state.userList} notify={this._updateUserList} placeholder='User Email' />
		return (
			<div className="home">
				<div className="introduction">
					<div className="tagline">
						<h1>Data Structures as a Service</h1>
						<h2>Development of data structures  &mdash; <span id='special'>in the cloud</span></h2>
						<span id="semi-line" />
						<SignIn />
					</div>
				</div>
				<div className='library-bindings'>
					<h1>Library Bindings</h1>
					<h3>To build programs with</h3>
					<a href='/library-binding/dsaas.jar' target='_blank'><button>Download Java Binding</button></a>
					<button onClick={e => {
						this.transitionTo('/tutorial/java');
					} }>Tutorial</button>
				</div>
				<div className='shared-datastructures'>
					<h2> View User's Publicly Shared Data Structures </h2>
					{this._renderDataStructures()}
				</div>
			</div>
		);
	},

	_updateUserList: function (userInfo) {

	},

	_renderDataStructures: function () {
		var ds = [];
		for (var datastructure in this.state.datastructures) {
			datastructure = this.state.datastructures[datastructure];
			ds.push(<DatastructureOverviewContainer datastructure={datastructure} namespace={datastructure.namespace} sharedview={true}/>)
		}
		if (ds.length == 0) {
			return "No public data structures available.";
		}
		return ds;
	},

	_onChange: function () {
		this.setState({datastructures: SharedPageStore.getDatastructures()});
	}
});