var React = require('react'),
	AuthStore = require('../../stores/AuthStore'),
	AuthActions = require('../../actions/AuthActions');

module.exports = React.createClass({
	getInitialState: function () {
		return AuthStore.getUser();
	},

	_submit: function (e) {
		e.preventDefault();
		var user = {};
		for (var label of ['email', 'name', 'namespace']) {
			user[label] = React.findDOMNode(this.refs[label]).value;
		}

		AuthActions.createUser(user);
	},

	render: function () {
		return (
			<div className='sign-up'>
				<h1>Registration</h1>
				<form name='sign-up'>
					<input type='text' ref='email' placeholder='email' value={this.state.email} disabled="true" /> <br />
					<input type='text' ref='name' placeholder='name' defaultValue={this.state.name} /> <br />
					<input type='text' ref='namespace' placeholder='namespace' /> <br />
					<input type='submit' ref='submit'  onClick={ this._submit }/>
				</form>
			</div>
		);
	}
});