var React = require('react'),
	JSONEditor = require('./JSONEditor.react');

module.exports = React.createClass({
	getInitialState: function () {
		return {
			json: JSON.parse(JSON.stringify(this.props.defaultValue))
		};
	},

	componentWillReceiveProps: function (nextProps) {
		this.setState({json: nextProps.defaultValue});
	},

	render: function () {
		console.log(this.state.json);
		return (<JSONEditor className='value' ref='value' 
			onChange={this._jsonChanged} key='editor' jsonKey='' 
			value={this.state.json} 
			create={this.onCreateJSON} jsonArrayPush={this._jsonArrayPush} />);
	},

	_jsonChanged: function (key, value) {
		let json = this.state.json;
		let jsonRoot = json;
		let parentJson, childKey;

		for (let i = key.length - 1; i > 0; i--) {
			parentJson = json;
			childKey = key[i];
			json = json[key[i]];
		}

		if (value === null) {
			delete parentJson[childKey];
		} else {
			let position = key[0];
			json[position] = value;
		}
		this.changeJSON(jsonRoot);
	},

	_jsonArrayPush: function (key, value) {
		let json = this.state.json;
		let jsonRoot = json;

		for (let i = key.length - 1; i >= 0; i--) {
			json = json[key[i]];
		}

		json.push(value);
		this.changeJSON(jsonRoot);
	},

	_jsonLabelChanged: function () {

	},

	changeJSON: function (json) {
		this.props.onChange(json);
		this.setState({json: json});
	}
});