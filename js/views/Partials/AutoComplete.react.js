var React = require('react');

module.exports = React.createClass({

	getDefaultProps: function () {
		return {
			limit: 3,
			placeholder: 'Auto-complete',
			active: false,
			startList: [
				'abcde',
				'abcdef',
				'abcdf',
				'abcdv',
				'abcd1',
			],
			list: []
		};
	},


	getInitialState: function () {
		return {
			list: []
		};
	},

	componentWillMount: function () { 
	},

	componentWillUnmount: function () { 
	},

	render: function () {
		var matchesFound = {'visibility': this.state.active ? 'visible' : 'hidden'};
		return (
			<span style={{position: 'relative'}} className='auto-complete'>
				<div className='possibleWords' style={matchesFound}>
					{this._displayElements()}
				</div>
				<input type='text' placeholder={this.props.placeholder} onChange={this._onChange} ref='autoComplete' />
			</span>
		);
	},

	_onChange: function (e) {
		var notify = this.props.notify || this._notify;
		if (e.target.value.length >= this.props.limit) {
			this.setState({active: true});
			notify(e.target.value);
		} else {
			this.setState({active: false});
		}
		this.props.onChange(e.target.value);
	},

	_setList: function (list) {
		if (list) {
			this.setState({list: list});
			if (list.length <= 1) {
				this.setState({active: false});
			}
		}
	},

	_displayElements: function () {
		var list = this.props.list || this.state.list || [];
		return list.map(word => {
			if (this.props.manipulate)
				word = this.props.manipulate(word);
			return (
				<div className='word' onClick={this._setWord}>
					<div className='primary'>{word.primary || word}</div>
					<div className='extra'>
						{word.extra}
					</div>
				</div>
				);
		});
	},

	_notify: function (val) {
		var list = [];
		for (var word in this.props.startList) {
			word = this.props.startList[word];
			if (word.indexOf(val) != -1) {
				list.push(word);
			}
		}
		this._setList(list);
	},

	_setWord: function (e) {
		var textField = React.findDOMNode(this.refs.autoComplete);
		textField.value = e.target.parentElement.getElementsByClassName('primary')[0].innerHTML;
		this.setState({active: false});
		this.props.onChange(textField.value);
	}
});