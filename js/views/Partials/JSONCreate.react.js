var React = require('react'),
	utils = require('../../utils');

module.exports = React.createClass({

	getInitialState: function () {
		return {
			activeButton: 'Object',
			active: this.props.active || false
		};
	},

	render: function () {

		var createStyle = {'display': this.state.active ? 'inline-block' : 'none'};
		var buttonStyle = {'display': this.state.active ? 'none' : 'inline-block'};
		var hide = {'display': 'none'};
		var labelStyle = {'display': this.props.hideLabel ? 'none': 'inline'};

		return (
			<div className='json-create'>
				<button className='success add-button' style={buttonStyle} onClick={e => {this.setState({active: true}); }}>+</button>
				<div style={createStyle}>
					<input type='text' placeholder='key' ref='input' 
						onChange={e => { this.setState({text: e.target.value}); }} style={labelStyle} value={this.state.text}
						/>
					<span className='button-group'>
						{this._renderButtonGroup()}
					</span>
					<button className='danger' onClick={this._onCancel}>Cancel</button>
				</div>
				<button className='danger remove-button' onClick={this._onRemove} style={this.props.parentKey ? buttonStyle : hide}>-</button>
			</div>
		);
	},

	_renderButtonGroup: function () {
		var buttons = ['Object', 'Array', 'String/Number'];

		var elements = [];
		for (let button of buttons) {
			elements.push(<button onClick={this._onCreate}>{button}</button>);
		}
		return elements;
	},


	_onCreate: function (e) {
		var text = this.state.text;
		if (!this.props.hideLabel) {
			if (text === '') return;
			else text += '';
		} else {
			text = this.props.parentKey;
		}

		if (/\W/.test(text)) {
			throw new Error('Invalid characters in input. Only alpha-numeric and underscore characters are allowed.');
		}

		this.props.create(text, e.target.innerHTML);
		this.setState({text: ''});
		e.target.parentNode.childNodes[0].value = '';

	},

	_onRemove: function () {
		var key;
		if (this.props.hideLabel) {
			key = this.props.parentKey;
			if (!utils.isArray(key))
				key = [key];
			key.splice(0, 0, '__');
		}
		this.props.remove(key);
	},

	_onCancel: function (e) {
		this.setState({active: false});
	}
});