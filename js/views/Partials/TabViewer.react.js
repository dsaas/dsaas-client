var React = require('react');

module.exports = React.createClass({

	getInitialState: function () {
		for (var label in this.props.tabs) {
			break;
		}
		if (this.props.activeTab) {
			label = this.props.activeTab;
		}
		return {
			active: label
		};
	},

	componentWillReceiveProps: function (nextProps) {
		if (nextProps.activeTab) {
			this.setState({active: nextProps.activeTab});
		}
	},

	render: function () {
		return (
			<div className='tab-viewer'>
				<div className='tab-labels'>
					{this._buildLabels()}
				</div>
				<div className='tab-content'>
					{this._buildViewer()}
				</div>
			</div>
		);
	},

	_buildLabels: function () {
		var labels = [];
		for (var label in this.props.tabs)
			labels.push((<button onClick={this._show} className={'tab-label ' + (label == this.state.active ? 'selected' : '')}>{label}</button>));
		return labels;
	},

	_buildViewer: function () {
		var content = [];
		var i = 0;
		for (var label in this.props.tabs) {
			content.push((
				<div className='tab-item' ref={'tabItem' + label} style={{display: this.state.active == label ? 'block' : 'none'}} >
					{this.props.tabs[label]}
				</div>
			));
			i++;
		}
		return content;
	},

	_show: function (e) {
		var label = e.target.innerHTML;
		this.setState({active: label});
		this.props.setActive(label);
	}
});