var React = require('react');

module.exports = React.createClass({
	render: function () {
		return (
			<div className='overlay' style={{
				position: 'absolute',
				'background-color': 'rgba(0,0,0,0.6)',
				top:0,
				bottom: 0,
				left: 0,
				right: 0,
				'z-index': 10
			}} onClick={this.props.onClick}></div>	
		);
	}
});