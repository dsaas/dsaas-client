var React = require('react');



var JSONDisplayer = React.createClass({

	render: function () {
		return (
			<div className='json-displayer'>
				{this._build(this.props.json)}
			</div>
		);
	},

	_build: function (obj) {
		var elements = [];
		for (var label in obj) {
			elements.push((
				<div className='json-element'>
					<span className='label'>{label}: &nbsp;</span>
					{this._extract(obj[label])}
				</div>
			));
		}
		return elements;
	},

	_extract: function (obj) {
		var element;
		if (typeof obj == 'string' || typeof obj == 'number')
			return (<span>{obj}</span>);
		else if (typeof obj == 'boolean')
			return (<span>{obj ? 'true' : 'false'}</span>);
		else if (typeof obj == 'number') {
			element = [];
			for (var el in obj) {
				element.push(this._extract(obj[el]));
			}
		} else element = this._build(obj);

		return (<div className='obj'>{element ? element : 'null'}</div>);
	}

});

module.exports = JSONDisplayer;