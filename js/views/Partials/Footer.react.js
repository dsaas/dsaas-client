

var React = require('react');
var ReactPropTypes = React.PropTypes;

var Footer = React.createClass({

  render: function() {
  	return (
  		<div className='footer-image'>
	        <img src='/images/panda_logo_black.png' />
	    </div>
    );
  }
});

module.exports = Footer;
