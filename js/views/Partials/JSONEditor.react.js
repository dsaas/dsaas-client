var React = require('react');
var JSONCreate = require('./JSONCreate.react');
var utils = require('../../utils');

var JSONEditor = React.createClass({

	getInitialState: function () {
		return {};
	},

	render: function () {
		return (
			<div>
				<div className='json-editor'>
					{this._build(this.props.value)}
					<JSONCreate create={this._onCreateJSON} key='__jc' remove={this._onRemove} parentKey={this.props.jsonKey}/>
				</div>
			</div>
		);
	},

	_build: function (obj) {
		var elements = [];
		for (var label in obj) {
			elements.push((
				<div className='json-element' key={label}>
					<input type='text' className='label' value={label} disabled={true} />
					{this._extract(label, obj[label])}
				</div>
			));
		}
		return elements;
	},

	_extract: function (key, obj) {
		var element;

		if (typeof obj == 'string' || typeof obj == 'number') {
			return (<span key={key}>
				<input type='text' defaultValue={obj} onChange={this._onChangeValue} data-key={key} />
				<button className='danger remove-button' onClick={e => {this._onRemoveElement(key)}}>-</button>
				</span>);
		} else if (typeof obj == 'boolean') {
			return (<span>{obj ? 'true' : 'false'}</span>);
		} else if (utils.isArray(obj)) {
			element = [];
			for (var el in obj) {
				let innerKey = utils.isArray(key) ? [el].concat(key) : [el, key];
				element.push(this._extract(innerKey, obj[el]));
			}
			element.push((<JSONCreate create={this._arrayPush} key='__jc' remove={this._onRemove} parentKey={key} hideLabel={true} />));
		} else {
			element = (<JSONEditor key={key} jsonKey={key} value={obj} onChange={this._onChildChange} remove={this._onRemove} jsonArrayPush={this._jsonArrayPush} />);
		}

		return (<div className='obj'>{element ? element : 'null'}</div>);
	},

	_onChildChange: function (key, value) {
		if (this.props.jsonKey) {
			key = key.concat(this.props.jsonKey);
		}
		this.props.onChange(key, value);
	},

	_onChangeValue: function (e) {
		var key = e.target.getAttribute('data-key');
		var value = e.target.value;
		this._onChildChange(key.split(','), value);
	},

	_onChangeLabel: function () {

	},

	_arrayPush: function (label, type) {
		var value;
		if (type === 'Object')
			value = {};
		else if (type === 'Array')
			value = [];
		else
			value = '';

		if (utils.isArray(label)) {
			if (this.props.jsonKey) {
				label = label.concat(this.props.jsonKey);
			}
		} else {
			label = [label];
			if (this.props.jsonKey) {
				label = label.concat(this.props.jsonKey);
			}
		}

		this.props.jsonArrayPush(label, value);
	},

	_jsonArrayPush: function (key, value) {
		this.props.jsonArrayPush(this.props.jsonKey ? key.concat(this.props.jsonKey) : key, value);
	},

	_onRemove: function (key) {
		if (key) {
			if (this.props.jsonKey)
				key = key.concat(this.props.jsonKey);
		} else {
			if (!key || !utils.isArray(key))
				key = [this.props.jsonKey];
		}
		this._onChildChange(key, null);
	},

	_onRemoveElement: function (key) {
		if (!utils.isArray(key)) {
			key = [key];
		}
		key = ['__'].concat(key);
		this._onRemove(key);
	},

	_onCreateJSON: function (label, type) {
		var value;
		if (type === 'Object')
			value = {};
		else if (type === 'Array')
			value = [];
		else
			value = '';

		this._onChildChange([label], value);
	}

});

module.exports = JSONEditor;