var ReactToastr = require('react-toastr'),
	React = require('react/addons'),
	{ToastContainer} = ReactToastr, // This is a React Element.
	ToastMessageFactory = React.createFactory(ReactToastr.ToastMessage.animation);

var stores = [];
stores.push(require('../../stores/AuthStore'));
stores.push(require('../../stores/JWTStore'));
stores.push(require('../../stores/AccessStore'));
stores.push(require('../../stores/DataStructureStore'));
stores.push(require('../../stores/WorkbenchStore'));
stores.push(require('../../stores/MapStore'));
stores.push(require('../../stores/GraphStore'));

module.exports = React.createClass({
	componentDidMount: function () {

		for (let store of stores) {
			store.onChange(this._alert);
		}

		window.onerror = (errormsg, url, line, column, obj) => {
			var json, message = obj.message;
			window.myMessage = obj;
			if (obj.response && obj.response.text) {
				try {
					json = JSON.parse(obj.response.text);
				} catch (e) {}
				if (json && json.error && json.error.message) message = json.error.message;
			}
			console.log('Showing alert: ', message);
			this._alert('Error: ' + message, 'error', 10000);
		}

		window.alert = this._alert;
	},

	_alert: function (msg, type='success', timeOut=4000) {
		if (!msg) return;
		this.refs.container[type](msg, '', {timeOut: timeOut, showAnimation: 'animated fadeIn', hideAnimation: 'animated fadeOut'});
	},
		
	render: function () {
		return (
			<div className='toastr'>
				<ToastContainer ref='container' toastMessageFactory={ToastMessageFactory} className='toast-top-right' />
			</div>
		);
	}
});