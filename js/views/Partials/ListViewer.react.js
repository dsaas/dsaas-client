var React = require('react');

module.exports =  React.createClass({

	render: function () {
		return (
			<div className='list-viewer'>
				<table>
				{this._build(this.props.list)}
				</table>
			</div>
		);
	},

	_build: function (list) {
		var elements = [];
		for (var i in list) {
			elements.push((
				<tr>
					{this._extract(list[i])}
				</tr>
			));
		}
		return elements;
	},

	_extract: function (item) {
		var cells = [];
		for (var el in item) {
			if (el == '__v') continue;
			cells.push((
				<td>{item[el]}</td>
			));
		}
		return cells;
	}

});