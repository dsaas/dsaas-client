var React = require('react'),
	AuthStore = require('../../stores/AuthStore'),
	AuthActions = require('../../actions/AuthActions'),
	HeaderStore = require('../../stores/HeaderStore'),
	HeaderActions = require('../../actions/HeaderActions');

function _setState() {
	var state = {};
	var user = AuthStore.getUser();
	var token = AuthStore.getToken();

	state.loggedin = !!token;

	// Set-up the user's link
	if (token) {
		state.username = user && user.name ? user.name : 'Unknown';
		if (state.username.length > 12) state.username = state.username.substring(0,9) + '...';
		state.SignOut = !this.state.signOut ? (<li onClick={e => { AuthActions.signOut(); }}>Sign Out</li>) : null;
	} else {
		state.SignOut = null;
	}

	this.setState(state);

	// Add the header elements
	this.setState({elements: HeaderStore.getHeaderElements() });
}

var Header = React.createClass({
	getInitialState: function () {
		return {
			elements: []
		};
	},

	componentWillMount: function () {
		_setState.apply(this);
		AuthStore.on('change', this._onChange);
		HeaderStore.onChange(this._onChange);
	},

	componentWillUnmount: function () {
		AuthStore.removeListener('change', this._onChange);
		HeaderStore.removeChangeListener(this._onChange);
	},

	render: function() {
		let userDropDown = (
			<div className='inline right nav-element-drop-down '>
				<div className='nav-element'>
					{this.state.username}
				</div>
				<div className='drop-down'>
					<ul>{this.state.SignOut}</ul>
				</div>
			</div>
			);

		if (!this.state.loggedin) {
			userDropDown = null;
		}

		return (
			<header id='header'>
				<div id='navbar'>
					{userDropDown}
					{this._showElements()}
					<a href={this._home()} className='inline'>
						<div id='brand'>DSaaS</div>
					</a>
				</div>
			</header>
		);
	},

	_onChange: function () {
		_setState.apply(this);

	},

	_home: function () {
		if (AuthStore.getToken()) {
			return '/workbench/';
		} else {
			return '/';
		}
	},

	_showElements: function () {
		var elements = [];
		for (let elementName in this.state.elements) {
			let element = this.state.elements[elementName];
			elements.push((<div onClick={element.fn} className='inline right nav-element'> {elementName} </div>));
		}
		return elements;
	}
});

module.exports = Header;
