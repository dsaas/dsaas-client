var React = require('react'),
	Header = require('./Partials/Header.react'),
	Footer = require('./Partials/Footer.react'),	
	Toastr = require('./Partials/Toastr.react'),
	RouteHandler = require('react-router').RouteHandler;

module.exports = React.createClass({
	render: function () {
		return (
			<div className='full-height'>
				<Toastr />
				<div className="overview-container full-height">
					<Header />
					<div className="container">
						<RouteHandler />
					</div>
					<Footer />
				</div>
			</div>
		);
	}
});