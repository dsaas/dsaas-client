var React = require('react'),
	Overlay = require('../Partials/Overlay.react');


module.exports = React.createClass({
	getInitialState: () => { 
		return {
			type: 'map',
			fileToImport: null
		}; 
	},

	render: function () {
		var actionButton = this.state.fileToImport ? 'Import' : 'Create';

		return (
		<div>
	    <Overlay onClick={this.props.event.cancel} />
	      <div className='datastructure-create-dialog dialog'>
	      	<div className='inner-center' style={{width: '310px'}}>
		      	<input type='text' placeholder='Datastructure Identifier' 
		      	onChange={e => {this.setState({id: e.target.value}); }} 
		      	value={this.state.id} />

		      	<select 
		      		onChange={e => {this.setState({type: e.target.value}); }} value={this.state.type}>
		      		<option value='map'>Map</option>
		      		<option value='graph'>Graph</option>
		      	</select>
		      	<div className='import-upload'>
			      	Optional Import: <input type='file' onChange={this._import} />
		      	</div>
		      	<div>
			      	<button onClick={this._create}>{actionButton}</button>
			      	<button onClick={this.props.event.cancel}>Cancel</button>
		      	</div>
		    </div>
	      </div>
	    </div>
		);
	},

	_create: function (e) {

		if (!this.state.id) {
			throw Error('No data structure identifier was given.');
			return;
		}


		if (this.state.fileToImport)
			this.props.event.import(this.state.id, this.state.type, this.state.fileToImport);
		else
			this.props.event.create(this.state.id, this.state.type);
	},

	_import: function (e) {
		// because the entire file is converted to a string and then sent to the
		var file = e.target.files[0];
		this.setState({fileToImport: file});
	}
});