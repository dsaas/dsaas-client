var React = require('react'),
	Router = require('react-router'),
	DataStructureActions = require('../../actions/DataStructureActions'),
	Navigation = require('react-router').Navigation,
	JSONEditorHolder = require('../Partials/JSONEditorHolder.react'),
	MapStore = require('../../stores/MapStore'),
	MapActions = require('../../actions/MapActions'),
	GraphActions = require('../../actions/GraphActions'),
	GraphStore = require('../../stores/GraphStore'),
	utils = require('../../utils');


var key = '', value = {};
var fromValue = '', toValue = '';

var EditView = React.createClass({
	
	mixins: [Navigation],
	
	getInitialState: function () {
		return {
			json: {}
		};
	},

	componentWillMount: function () {

		if (this.props.type == 'Map') {
			MapStore.onChange(this._mapChangeListener);
		}

		if (this.props.type == 'Graph') {
			GraphStore.onChange(this._graphChangeListener);
		}

		var p = this.props.params;
		if (p.key) {
			key = p.key;
			if (this.props.type == 'Map') {
				let map = MapStore.getMap(p.version);
				if (map && map[p.key]) {
					this._mapChangeListener();
				} else {
					MapActions.fetchSingleEntry(p.namespace, p.id, p.version, p.key);
				}
			} else {
				let graph = GraphStore.getGraph(p.version);
				if (graph && graph[p.objType][p.key]) {
					this._graphChangeListener();
				} else {
					GraphActions.fetch(p.namespace, p.id, p.version, p.key, p.objType);
				}
			}
		}

		// Setup Listeners
		var self = this;
		GraphActions.onEvents({
			ADDED_NODE: function (v, k, val, body) {
				self.transitionTo(self.props.type.toLowerCase(), {
					namespace: self.props.params.namespace,
					id: self.props.params.id,
					version: body.version
				});
			},
			ADDED_EDGE: function (version, fromValue, toValue, value, body) {
				self.transitionTo(self.props.type.toLowerCase(), {
					namespace: self.props.params.namespace,
					id: self.props.params.id,
					version: body.version
				});
			}
		});

		DataStructureActions.onEvents({
			NEW_VERSION_ARRIVED: function (version, newVersion) {
				self.transitionTo(self.props.type.toLowerCase(), {
					namespace: self.props.params.namespace,
					id: self.props.params.id,
					version: newVersion
				});
			}
		})
	},

	componentWillUnmount: function () {

		if (this.props.type == 'Map') {
			MapStore.removeChangeListener(this._mapChangeListener);
		}

		if (this.props.type == 'Graph') {
			GraphStore.removeChangeListener(this._graphChangeListener);
		}

	},


	render: function() {
		
		var accept = this.props.params.key ? 'Save' : 'Create';
		var msg = this.props.params.namespace + ' / ' + this.props.params.id + ' / ' + this.props.params.version;
		var message = null;
		
		if (this.props.type == 'Map')
			message = !this.props.params.key ? (<h1>Adding object to {msg}</h1>) : (<h1> Editing element in {msg}</h1>);
		else
			message = !this.props.params.key ? (<h1>Adding {this.props.params.objType} to {msg}</h1>) : (<h1>Editing {this.props.params.objType} in {msg}</h1>);

		var header = (<input type='text' placeholder='Key' defaultValue={this.props.params.key} className='key' ref='key' 
						onChange={e=>{key = e.target.value;}}/>);
		if (this.props.params.objType == 'edges') {
			if (this.props.params.key) {
				let mark = this.props.params.key.indexOf('-->');
				fromValue = this.props.params.key.substring(0, mark);
				toValue = this.props.params.key.substring(mark+3);
			}

			header = (<div>
					<input type='text' placeholder='From' className='from' onChange={e => {fromValue = e.target.value;}} defaultValue={fromValue} />
					<input type='text' placeholder='To' className='to' onChange={e => {toValue = e.target.value;}} defaultValue={toValue} />
				</div>);
		}

		return (
			<div className='edit-overlay'>
				<div className='header'>
					{message}
					{header}
					<button onClick={e=> {this._goBack()}}> &lt; Back </button>
					<div className='right'>
						<button onClick={this._putObject} className='success'>{accept}</button>
						<button onClick={this._removeObject} className='danger'>Remove</button>
					</div>
				</div>
				<JSONEditorHolder onChange={this._onChange} defaultValue={this.state.json}/>
			</div>
		);
	},

	_putObject: function (e) {
		if (key == '' && (fromValue == '' || toValue == '')) throw Error('Please add a key or a to and from value');
		this.props.put(e, this.props.params, key, value, fromValue, toValue);
		// this._goBack();
	},

	_removeObject: function (e) {
		this.props.remove(e, this.props.params, key);
		this._goBack();
	},

	_onChange: function (val) {
		value = val;
	},

	_goBack: function () {
		this.transitionTo(this.props.type.toLowerCase(), {
			namespace: this.props.params.namespace,
			id: this.props.params.id,
			version: this.props.params.version
		});
	},

	_mapChangeListener: function () {
		var p = this.props.params;
		var map = MapStore.getMap(p.version);
		var json = map && map[p.key];
		value = json;
		this.setState({json: json});
	},

	_graphChangeListener: function () {
		var p = this.props.params;
		var graph = GraphStore.getGraph(p.version);
		var json;
		if (p.objType == 'nodes') {
			json = graph && graph[p.objType] && graph[p.objType][p.key] && graph[p.objType][p.key].props;
		} else {
			json = graph && graph[p.objType] && graph[p.objType][p.key];
			delete json['from'];
			delete json['to'];
			delete json['id'];
		}
		value = json;
		this.setState({json: json});
	}
});

module.exports = {
	Map: utils.wrap(EditView, {
		type: 'Map',
		put: function (e, params, key, value) {
			DataStructureActions.putObject(
				params.namespace,
				params.id,
				params.version,
				key, 
				value
			);
		},
		remove: function (e, params, key) {
			MapActions.removeObject(
				params.namespace,
				params.id,
				params.version,
				key
			);
		}
	}),
	Graph: utils.wrap(EditView, {
		type: 'Graph',
		put: function (e, params, key, value, fromValue, toValue) {
			if (params.objType == 'nodes') {
				GraphActions.addGraphNode(
					params.namespace,
					params.id,
					params.version,
					key,
					value);
			} else if (params.objType == 'edges') {
				GraphActions.addGraphEdge(
					params.namespace,
					params.id,
					params.version,
					fromValue,
					toValue,
					value);
			}
		},
		remove: function (e, params, key) {
			GraphActions.remove(
				params.namespace,
				params.id,
				params.version,
				key,
				params.objType);
		}
	})
};