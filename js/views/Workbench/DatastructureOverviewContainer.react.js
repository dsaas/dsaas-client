var React = require('react');
var Navigation = require('react-router').Navigation;
var AuthStore = require('../../stores/AuthStore');

module.exports = React.createClass({
	mixins: [Navigation],
	render: function () {
		var identifier = {id: this.props.datastructure.datastructureId, namespace: this.props.namespace};
		var isAdmin = this.props.datastructure.access == 'admin';
		var hideOnSharedView = this.props.sharedview ? {'display': 'none'} : {};

		var permissionButton = (
			<button onClick={e => {this._go(e, 'access', identifier)}} style={hideOnSharedView}>
					Permissions
			</button>);

		if (!isAdmin) {
			permissionButton = null;
		}

		return (
	      <div className='datastructure-overview-container' onClick={this._view} key={this.props.datastructure.namespace + '/' + this.props.datastructure.datastructureId}>
			<div className='button-group'>
				<button onClick={this._fork} style={hideOnSharedView}>Fork</button>
				{permissionButton}
				<a href={'/api/' + this.props.datastructure.namespace + '/' + this.props.datastructure.datastructureId + '/export?authorization=' + AuthStore.getToken()} download='export.json'><button onClick={e => {e.stopPropagation();}}>
				Export
				</button></a>
				<button onClick={e => {this._go(e, 'info', identifier) }}>Info</button>
			</div>
	      	<div className='datastructureId'>{this.props.datastructure.datastructureId}</div>
	      	<div className='type'>{this.props.datastructure.type}</div>
	      </div>			
		);
	},
				// <button onClick={e => {this._go(e, '', {}) }} style={hideOnSharedView}>Remove</button>

	_view: function (e) {
		this.transitionTo(this.props.datastructure.type, {
			id: encodeURIComponent(this.props.datastructure.datastructureId),
			namespace: this.props.namespace
		});
	},

	_go: function (e, path, prop) {
		e.stopPropagation();
		this.transitionTo(path, prop);
	},

	_fork: function (e) {
		e.stopPropagation();
		this.props.fork(this.props.datastructure.namespace, this.props.datastructure.datastructureId);
	}

});