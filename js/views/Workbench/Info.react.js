var React = require('react'),
	InfoActions = require('../../actions/InfoActions'),
	InfoStore = require('../../stores/InfoStore'),
	ListViewer = require('../Partials/ListViewer.react');

module.exports = React.createClass({

	getInitialState: function () {
		return {};
	},

	componentWillMount: function () {
		InfoActions.getLogs(this.props.params.namespace, this.props.params.id);
		InfoStore.onChange(this._onChange);
	},

	componentWillUnmount: function () {
		InfoStore.removeChangeListener(this._onChange);
		InfoStore.clear();
	},

	render: function () {
		return (
			<div className='info'>
				<h1>Information of {this.props.params.namespace + '/' + this.props.params.id}</h1>
				<div className='logs'>
					<h2>Logs</h2>
					<ListViewer list={this.state.logs} />
				</div>
			</div>
		);
	},

	_onChange: function () {
		this.setState({logs: InfoStore.getLogs()});
	}

});