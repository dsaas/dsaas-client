var React = require('react'),
	Overlay = require('../Partials/Overlay.react');

module.exports = React.createClass({
	getInitialState: function () { 
		return {
		}; 
	},

	componentDidMount: function () {
		React.findDOMNode(this.refs.forkDialog).focus();
	},

	render: function () {
		return (
			<div>
				<Overlay onClick={this.props.event.cancel} />
				<div className='fork-dialog dialog'>
					<input type='text' placeholder='Datastructure Identifier' 
					onChange={e => {this.state.id = e.target.value; }} 
					value={this.state.id} ref='forkDialog'/>

					<button onClick={this._fork}>Fork</button>
					<button onClick={this.props.event.cancel}>Cancel</button>
				</div>
			</div>			
		);
	},

	_fork: function (e) {
		this.props.event.fork(this.state.id);
		this.props.event.cancel();
	}

});