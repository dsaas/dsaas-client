var React = require('react'),
	Router = require('react-router'),
	MapStore = require('../../stores/MapStore'),
	TabViewer = require('../Partials/TabViewer.react'),
	MapActions = require('../../actions/MapActions'),
	VersionHistory = require('./VersionHistory.react'),
	Navigation = require('react-router').Navigation,
	DisplayJSON = require('../Partials/DisplayJSON.react'),
	HeaderActions = require('../../actions/HeaderActions'),	
	utils = require('../../utils');


var version = 0;

module.exports = React.createClass({
	
	mixins: [Navigation],
	
	getInitialState: function () {
		return {
			version: this.props.params.version || 0,
			filter: ''
		};
	},

	componentWillMount: function () {
		MapStore.setData({namespace: this.props.params.namespace, id: this.props.params.id});
		MapStore.onChange(this._onChange);
		MapStore.getVersion(this.state.version);
		// this._changeVersion(version);
	},

	componentWillUnmount: function () {
		MapStore.removeChangeListener(this._onChange);
		version = this.state.version;
	},

	render: function() {
		return (
			<div className='datastructure-overlay'> 
				<TabViewer
					tabs={{
						'Version History': (<VersionHistory changeVersion={this._changeVersion} params={this.props.params} defaultVersion={this.state.version}/>),
					}} />
				<div className='datastructure-container'>
					<h2>{this.props.params.namespace + '/' + this.props.params.id + '/' + utils.trunc(this.state.version, 4, 4)}</h2>
					<div className='tool-bar'>
						<button onClick={this._openEdit.bind(this, null)} name='add'>Add Key-Value Pair<span className='right'>+</span></button>
						<input type='text' placeholder='Filter Keys' onChange={this._filter}/>
					</div>
					<div className='data-struture'>
						{this._renderMap()}
					</div>
				</div>
			</div>
		);
	},

	_changeVersion: function (version) {
		this.setState({version: version});
		MapStore.getVersion(version);
	},

	_onChange: function () {
		var map = MapStore.getMap();
		this.setState(map);
	},

	_filter: function (e) {
		this.setState({filter: e.target.value});
	},

	_renderMap: function () {
		if (this.state.map) {
			var entries = [];
			for (let key in this.state.map) {
				if (this.state.filter != '') {
					if (key.toLowerCase().indexOf(this.state.filter.toLowerCase()) == -1) continue;
				}
				entries.push(
					<div className="map-entry" key={key} onClick={this._openEdit.bind(this, key)}>
						<div className="key">{key}</div>
						<div className="value">
							<DisplayJSON json={this.state.map[key]} />
						</div>
					</div>
				);
			}
			return entries;
		} else {
			return (<div>No Map data available.</div>);
		}
	},

	_openEdit: function (key, e) {
		VersionHistory.setRemove(false);
		this.transitionTo('editMap', {
			namespace: this.props.params.namespace, 
			id: this.props.params.id,
			version: this.state.version,
			key: key,
			type: 'map'
		});
	}
});