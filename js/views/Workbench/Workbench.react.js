var React = require('react');

var	DatastructureOverviewContainer = require('./DatastructureOverviewContainer.react'),
	CreateDialog = require('./CreateDialog'),
	ForkDialog = require('./ForkDialog.react'),
	WorkbenchActions = require('../../actions/WorkbenchActions'),
	AuthStore = require('../../stores/AuthStore'),
	Navigation = require('react-router').Navigation,
	WorkbenchStore = require('../../stores/WorkbenchStore');

var forkOriginalId, forkNewId, forkOriginalNamespace, forkNewNamespace;

function getWorkbenchState () {
	return {
		datastructures: WorkbenchStore.getDatastructures(),
		namespace: AuthStore.getUser().namespace,
		filter: '',
		filterType: ''
	};
}

module.exports = React.createClass({

	mixins: [Navigation],

	getInitialState: function () { return getWorkbenchState(); },

	componentDidMount: function () {
		WorkbenchActions.populateWorkbenchStore();
		WorkbenchStore.on('change', this._onChange);
		AuthStore.on('change', this._onChange);
	},

	componentWillUnmount: function () {
		WorkbenchStore.removeListener('change', this._onChange);
		AuthStore.removeListener('change', this._onChange);
	},

	render: function () {
		var activated = {};
		if (!this.state.filterType)
			activated['both'] = 'selected';
		else
			activated[this.state.filterType] = 'selected';

		return (
				// <a href='/library-binding/dsaas.jar' target='_blank'><button>Download Java Binding</button></a>
				// 	<button onClick={e => {
				// 		this.transitionTo('/tutorial/java');
				// 	} }>Tutorial</button>
				
			<div className='workbench'>
				<h2>Workbench</h2>

				<input type='text' placeholder='Filter' onChange={this._filter} />
				<button onClick={e => { this._filterType('graph'); }} className={activated['graph']}>Graphs</button>
				<button onClick={e => { this._filterType('map'); }} className={activated['map']}>Maps</button>
				<button onClick={e => { this._filterType(); }} className={activated['both']}>Both</button>

				<br></br>

				<button onClick={e => { this._dialogDisplay(true); }} className='add-data-structure'>Add Data Structure +</button>
				<button onClick={e => { this.transitionTo('/tokens/'); }}>Access Tokens</button>
				{this.state.showCreateDialog ? 
					(<CreateDialog 
						event= {{
								create: this._createDataStructure,
								import: this._importDataStructure,
								cancel: e => this.setState({showCreateDialog: false})
							}} />) 
				: null}

				{this.state.showForkDialog ? 
					(<ForkDialog
						event= {{
							fork: this._fork, 
							cancel: e => this.setState({showForkDialog: false})
						}} />) 
				: null}
				{this._renderDatastructures()}
			</div>
		);
	},

	_renderDatastructures: function () {
		if (this.state.datastructures) {
			var self = this;
			var list = [];
			for (var namespace in this.state.datastructures) {
				list.push((
					<div>
						<div className='namespace'>{namespace}</div>
						{() => {
							return this.state.datastructures[namespace].map(function (ds) {
								if (self.state.filterType != '') {
									if (self.state.filterType != ds.type) return null;
								}
								if (self.state.filter != '') {
									if (ds.datastructureId.toLowerCase().indexOf(self.state.filter.toLowerCase()) == -1) return null;
								}
								return (<DatastructureOverviewContainer datastructure={ds} namespace={namespace} fork={self._showFork} />);
							});
						}()}
					</div>));
			}
			return list;
		} else {
			return (<div>No data structures associated with namespace "{this.state.namespace}" </div>);
		}
	},

	_onChange: function () {
		this.setState(getWorkbenchState());
	},

	_dialogDisplay: function (choice) {
		this.setState({showCreateDialog: choice});
	},

	_createDataStructure: function (id, type) {
		WorkbenchActions.createDatastructure(AuthStore.getUser().namespace, id, type);
		this._dialogDisplay(false);
	},

	_importDataStructure: function (id, type, file) {
		WorkbenchActions.importDatastructure(AuthStore.getUser().namespace, id, type, file);
		this._dialogDisplay(false);
	},

	_showFork: function (namespace, datastructureId) {
		forkOriginalNamespace = namespace;
		forkOriginalId = datastructureId;
		this.setState({showForkDialog: true});
	},

	_fork: function (id) {
		WorkbenchActions.fork(forkOriginalNamespace, forkOriginalId, AuthStore.getUser().namespace, id);
	},

	_filter: function (e) {
		this.setState({filter: e.target.value});
	},

	_filterType: function (type) {
		if (!type) type = '';
		this.setState({filterType: type});
	},

	statics: {
		willTransitionTo: function (transition, params) {
			if (!AuthStore.getToken()) {
				transition.redirect('/');
			}
		}
	}

});