var React = require('react'),
	Cytoscape = require('cytoscape'),
	cydagre = require('cytoscape-dagre'),
	dagre = require('dagre'),
	DataStructureStore = require('../../stores/DataStructureStore'),
	DataStructureActions = require('../../actions/DataStructureActions'),
	utils = require('../../utils'),
	Router = require('react-router');

var cy, target;
var currentNode = 0,
	mergeNodeA = null, mergeNodeB = null,
	defaultColour = '#333',
	maximized = false,
	mergeColour = '#28CF0C',
	highlightColour = '#FF5722',
	firstBuild;

var namespace, id, remove = true, trackingVersion = null;
var cyReady = false, dataReady = false, firstVersion;

function applyStyle(element, map) {
	for (var prop in map) {
		element.style[prop] = map[prop];
	}
}


module.exports = React.createClass({

	getInitialState: function () {
		return {
			mergeActive: false,
			trackingVersion: null
		};
	},

	componentWillMount: function () {
		cyReady = false;
		dataReady = false;
		remove = true;
		firstBuild = true;

		namespace = this.props.params.namespace;
		id = this.props.params.id;
		if (!namespace && !id) throw new Error('VersionHistory mounted without a data structure.');

		DataStructureActions.getVersionHistory(namespace, id);
		DataStructureActions.registerSocket(namespace, id);
		DataStructureStore.onChange(this._onChange);
		firstVersion = this.props.defaultVersion;
	},

	componentDidMount: function () {
		cydagre( Cytoscape, dagre );
		cy = Cytoscape({
			container: document.getElementById('cy'),
			style: Cytoscape.stylesheet()
				.selector('node').css({
					'content': 'data(label)',
					'text-valign': 'center',
			        'text-halign': 'center',
			        'font-size': '8px',
			        'background-color': defaultColour,
			        'color': '#fff'
				})
				.selector('edge').css({
					// 'target-arrow-shape': 'triangle',
					'line-color': '#222'
					// 'target-arrow-color': '#ddd'
				}),

			layout: {
				name: 'dagre',
				nodeSep: 16,
				rankSep: 16,
				padding: 1,
				animate: true,
				animationDuration: 200,
				fit: false
			},
			motionBlur: true
			// autolock: true,
		});
		cy.on('tap', 'node', this._updateVersion);
		// this._drawGraph(DataStructureStore.getVersionHistory());
	},

	componentWillUnmount: function () {
		if (remove) {
			DataStructureStore.removeChangeListener(this._onChange);
			DataStructureStore.remove();
			DataStructureActions.unregisterSocket();
		}
	},

	render: function() {
		var mergeButton = this.state.mergeActive ? "Merge Activated" : "Merge" ;
		var trackStyle = this.state.trackingVersion ? {display: 'block'} : {display: 'none'};
		return (
			<div className='version-history'>
				<div id="cy" className='version-history-view'></div>
				<div className="button-group">
					<button id='resize-button-cy' onClick={this._resize}>[ ]</button>
					<button id='merge-button' onClick={this._mergeActive}>{mergeButton}</button>
					<button id='tracker-button' onClick={this._track}>Track</button>
				</div>
				<span style={trackStyle} className="track-info">Tracking: {utils.trunc(this.state.trackingVersion, 4, 4)}</span>
			</div>
		);
	},

	_onChange: function (a, b) {
		var data = DataStructureStore.getVersionHistory();
		this._drawGraph(data);

		if (firstVersion) {
			target = cy.getElementById(firstVersion);
			cy.center(target);
			target.style('background-color', highlightColour);
			firstVersion = null;
		}

		if (DataStructureStore.lastVersionAdded()) {
			var nextVersion = DataStructureStore.lastVersionAdded();
			// console.log(this.state.trackingVersion, data[nextVersion].parents);
			console.log(this.state.trackingVersion);
			console.log(data[nextVersion].parents.indexOf(this.state.trackingVersion));
			console.log(data[nextVersion].parents);

			if (this.state.trackingVersion &&
				data[nextVersion].parents.indexOf(this.state.trackingVersion) != -1) {

				console.log('update tracking version');
				this.setState({trackingVersion: nextVersion});
				this._internalVersionUpdate(nextVersion);
			}
		}
	},

	_drawGraph: function (data, startNode) {
		if (cy && data) {

			let graph = {nodes: {}, edges: {}};
			let start = window.performance.now();
			let startNodes = [];
			let maxHeight = 4;

			if (startNode) {
				if (data[startNode].children.length > 0)
					startNodes = data[startNode].children;
				else
					startNodes = [startNode];
				maxHeight = 4;
			} else {
				for (let node in data) {
					if (data[node].children.length == 0) {
						startNodes.push(node);
					}
				}
			}

			this._buildGraphData(startNodes, data, graph, maxHeight);

			// console.log(Object.keys(data).links.length);
			console.log('Time: ' + (window.performance.now() - start));

			for (let n in graph.nodes) {
				let node = graph.nodes[n];
				if (!cy.getElementById(node.data.id).isNode()) {
					cy.add(node);
				}
			}

			for (let e in graph.edges) {
				let edge = graph.edges[e];
				if (!cy.getElementById(edge.data.id).isEdge()) {
					cy.add(edge);
				}
			}
		}

		cy.elements().layout({name: 'dagre'});

	},

	_buildGraphData: function (startNodes, data, graph, maxHeight) {
		var label = 0;
		let queue = [];
		let width = 0, height = 0;

		for (let node of startNodes) {
			queue.push([node, 0, 0]);
			while (queue.length > 0) {
				let nwh = queue.shift();

				let width = nwh[1];
				let height = nwh[2];
				node = String(nwh[0]);

				if (!graph.nodes[node]) {
					graph.nodes[node] = {
						data: {id: node, label: utils.trunc(node, 2, 2)},
						group: 'nodes',
						position: { x: width, y: height }
					};

					if (height < maxHeight) {
						for (let c in data[node].parents) {
							let child = data[node].parents[c];
							let edgeId = node + ',' + child;
							if (!graph.edges[edgeId]) {
								graph.edges[edgeId] = {
									group: 'edges',
									data: {id: edgeId, source: node, target: child}
								};
								queue.push([child, ++width, height + 1]);
							}
						}
					}
				}
			}
		}
	},

	_updateVersion: function (e) {
		if (this.state.mergeActive) {
			if (!mergeNodeA) {
				mergeNodeA = e.cyTarget.id();
				cy.getElementById(mergeNodeA).style('background-color', mergeColour);
			} else {
				mergeNodeB = e.cyTarget.id();
				cy.getElementById(mergeNodeB).style('background-color', mergeColour);

				if (mergeNodeA != mergeNodeB) {
					DataStructureActions.merge(this.props.params.namespace,
						this.props.params.id, mergeNodeA, mergeNodeB);
				}

				cy.getElementById(mergeNodeA).style('background-color', defaultColour);
				cy.getElementById(mergeNodeB).style('background-color', defaultColour);

				mergeNodeA = null;
				mergeNodeB = null;
				this.state.mergeActive = false;
			}
		} else {

			// Update the graph
			this._internalVersionUpdate(e.cyTarget.id());
		}
	},

	_internalVersionUpdate: function (versionId) {
		cy.getElementById(currentNode).style('background-color', defaultColour);
		currentNode = versionId;
		this._drawGraph(DataStructureStore.getVersionHistory(), currentNode);

		target = cy.getElementById(currentNode);
		// cy.animate({center: {eles: target}, {duration: 1000});
		cy.center(target);
		target.style('background-color', highlightColour);

		this.props.changeVersion(currentNode);
	},

	_resize: function (e)  {
		var cyContainer = document.getElementById('cy');
		var resizeButtonCy = document.getElementById('resize-button-cy');
		if (!maximized) {
			applyStyle(cyContainer, {position: 'absolute', top: 0, left: 0, zIndex: 1000, width: '100%', height: '100%'});
			applyStyle(resizeButtonCy, {position: 'absolute', bottom: '10px', left: '10px', zIndex: 1001});
		} else {
			applyStyle(cyContainer, {position: 'initial', zIndex: 0, width: '400px', height: '400px'});
			applyStyle(resizeButtonCy, {position: 'initial', zIndex: 0, bottom:0, left: 0});
		}

		maximized = !maximized;
		cy.resize();
		if (target) cy.center(target);
	},

	_track: function (e) {
		this.setState({trackingVersion: currentNode});
	},

	_mergeActive: function (e) {
		this.setState({mergeActive: !this.state.mergeActive});
	},

	statics: {
		setRemove: function (r) {
			remove = r;
		}
	}

});