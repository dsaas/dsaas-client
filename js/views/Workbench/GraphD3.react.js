var React = require('react'),
	Router = require('react-router'),
	d3 = require('d3'),
	TabViewer = require('../Partials/TabViewer.react'),
	DisplayJSON = require('../Partials/DisplayJSON.react'),
	VersionHistory = require('./VersionHistory.react'),
	GraphStore = require('../../stores/GraphStore'),
	Navigation = require('react-router').Navigation,
	GraphActions = require('../../actions/GraphActions'),
	utils = require('../../utils');

var svg, defaultColour = '#607D8B', num = 1,
	width, height, force, nodes, links,
	edgeLabel = 'id',
	_data = {},
	_labelName,
	nodeSet = {}, linkSet = {};

var constant = {
	versionHistory: 'Version History',
	propertyViewer: 'Property Viewer',
	graphEditor: 'Graph Editor'
};


module.exports = React.createClass({

	mixins: [Navigation],
	
	getInitialState: function () {
		return {
			version: this.props.params.version || 0,
			labelName: 'id',
			loading: false,
			nodeProps: null,
			nodeId: null,
			activeTab: 'Version History',
			elementType: null
		};
	},

	componentWillMount: function () {
		GraphStore.setData(this.props.params);
		GraphStore.onChange(this._onChange);
		GraphStore.getVersion(this.state.version);
	},

	componentDidMount: function () {
		svg = d3.select("#graph-structure");

		let bounds = svg.node().getBoundingClientRect();
		width = bounds.width;
		height = bounds.height;
		force = d3.layout.force();
		nodes = force.nodes();
		links = force.links();
	},

	componentWillUnmount: function () {
		GraphStore.removeChangeListener(this._onChange);
		svg = null;
	},

	render: function() {
		var showLoading = this.state.loading ? {display: 'block'} : {display: 'none'};
		
		var tabs = {};
		tabs[constant.versionHistory] = (<VersionHistory changeVersion={this._changeVersion} params={this.props.params} defaultVersion={this.state.version}/>);
		var changeButton = this.state.nodeId ? (<button onClick={this._createNode.bind(this, this.state.elementType)}>Change</button>) : null;
		var noPropertyToView = !!this.state.nodeProps ? '' : (
			<div className='msg'>
			No node has been clicked on, so no property information is available.  Please select a node from the graph view.  If none is available, please use the version history to select a version first.
			</div>);
		tabs[constant.propertyViewer] = (
						<div className='property-viewer'>
							{noPropertyToView}
							{changeButton}
							<DisplayJSON json={this.state.nodeProps || {}} />
						</div>);

		tabs[constant.graphEditor] = (
			<div className='graph-editor'>
				<div className="button-group">
					<button onClick={this._addNewItem.bind(this, 'nodes')}>Create Node +</button>
					<button onClick={this._addNewItem.bind(this, 'edges')}>Create Edge +</button>
				</div>
				<table>
					<tr>
						<td>Label For Node</td>
						<td><input type='text' defaultValue={this.state.labelName} onBlur={this._changeLabel}/></td>
					</tr>
					<tr>
					</tr>
				</table>
			</div>);

		return (
			<div className='datastructure-overlay'>
				<TabViewer tabs={tabs}  activeTab={this.state.activeTab} setActive={label => {this.setState({activeTab: label}); }} />
				<span style={showLoading}>Loading...</span>
				<div className='datastructure-container'>
					<h2>{this.props.params.namespace + '/' + this.props.params.id + '/' + utils.trunc(this.state.version, 5, 5)}</h2>
					<div className='graph-structure'>
						<svg id='graph-structure'></svg>
					</div>
				</div>
			</div>
						// <td>Label For Edge</td>
						// <td><input type='text' defaultValue={this.state.edgeLabel} onBlur={this._changeEdgeLabel}/></td>
		);
	},

	_addNewItem: function (objType) {
		this._createNode(objType, 'RANDOM_STRING');
	},

	_createNode: function (objType, n) {
		VersionHistory.setRemove(false);
		var key = n == 'RANDOM_STRING' ? null :  this.state.nodeId && encodeURIComponent(this.state.nodeId) || null; 
		this.transitionTo('editGraph', {
			namespace: this.props.params.namespace, 
			id: this.props.params.id,
			version: this.state.version,
			key: key,
			type: 'graph',
			objType: objType
		});
	},

	_addNode: function (id) {
		nodes.push({id: id, value: nodes.length});
		// this._updateLayout();
	},

	_removeNodes: function (toRemove) {
		for (let n in toRemove) {
			this._removeNode(n);
			delete nodeSet[n];
		}
		// this._updateLayout();
	},

	_removeNode: function (id) {
        nodes.splice(this._findNodeIndex(id), 1);
        // this._updateLayout();
	},

	_findNode: function (id) {
		for (var i in nodes)
			if (nodes[i].id === id) return nodes[i];
		return null;
	},

	_findNodeIndex: function (id) {
		for (var i = 0; i < nodes.length; i++)
			if (nodes[i].id == id) return i;
		return -1;
	},

	_removeLinks: function (toRemove) {
		for (var i = 0; i < links.length; i++) {
			var str = links[i].source.id + '-->' + links[i].target.id;
            if (toRemove[str]) {
            	delete linkSet[str];
            	toRemove[str] = i;
            }
        }

       	let removed = 0;
        for (let l in toRemove) {
	    	links.splice(toRemove[l] - removed, 1);
	    	removed++;
        }
        // this._updateLayout();
	},

	_addLink: function (source, target, id, value) {
		links.push({source: this._findNode(source), target: this._findNode(target), value: value, id: id});
        // this._updateLayout();
	},

	_findLink: function (source, target) {
		for (var l in links) {
			l = links[l];
			if (l.source.id == source && l.target.id == target) return l;
		}
		return null;
	},

	_removeAllLinks: function () {
        links.splice(0, links.length);
	},

    _removeAllNodes: function () {
        nodes.splice(0, nodes.length);
    },

	_changeVersion: function (version) {
		this.setState({version: version});
		GraphStore.getVersion(version);
	},

	_renderGraph: function (data, fullEdges) {
		if (data && fullEdges) {

			_data = data;
			num++;

			for (let nodeId in data) {
				if (!this._findNode(nodeId)) {
					this._addNode(nodeId);
				}

				nodeSet[nodeId] = num;

				for (let edge in data[nodeId].edges) {
					
					let mark = edge.indexOf('-->');
					let source = edge.substring(0, mark);
					let target = edge.substring(mark+3);

					// Add the source if it has not been added yet
					if (!this._findNode(source)) {
						this._addNode(source);
					}
					
					nodeSet[nodeId] = num;

					// Add the target if it has not been added yet
					if (!this._findNode(target)) {
						this._addNode(target);
					}

					nodeSet[nodeId] = num;

					// Add the link if it has not been added yet
					var link = this._findLink(source, target);
					var value = fullEdges && fullEdges[edge][edgeLabel] || null;
					console.log(value);
					if (!link) {
						this._addLink(source, target, edge, value);
					} else {
						link.value = value;
					}

					linkSet[edge] = num;
				}
			}

			// Remove the unecessary links and nodes
			var toRemove = [];
			for (let l in linkSet)
				if (linkSet[l] != num)
					toRemove[l] = 1;

			this._removeLinks(toRemove);

			toRemove = [];
			for (let n in nodeSet)
				if (nodeSet[n] != num)
					toRemove[n] = 1;

			this._removeNodes(toRemove);

			// Update the representation
			this._updateLayout();
			
			if (num == 5) num = 1;

		}
	},

	_updateLayout: function () {
		var self = this;
		var colorScale = d3.scale.category10();
		var link = svg.selectAll("line")
			.data(links);

		var enterLink = link.enter()
			.append("line")
			.on('click', this._showViewer)
			.attr("class", "link")
			.attr('marker-end','url(#arrowhead)');

		enterLink.on('click', this._showViewer);
					
		link.exit().remove();

		var linkpath = svg.selectAll(".linkpath")
			.data(links)
			.enter()
			.append('path')
			.attr({
				'd': function(d) {
					if (d && d.source && d.source.x && d.source.y && d.target && d.target.x && d.target.y)
						return 'M '+d.source.x+' '+d.source.y+' L '+ d.target.x +' '+d.target.y
				},
				'class':'linkpath',
				'fill-opacity':0,
				'stroke-opacity':0,
				'fill':'blue',
				'stroke':'red',
				'id':function(d,i) {return 'linkpath'+i}})
			.style("pointer-events", "none");

		var edgelabels = svg.selectAll(".edgelabel")
			.data(links)
			.enter()
			.append('svg:text')
			.style("pointer-events", "none")
			.attr({'class':'edgelabel',
				'id':function(d,i) {
					console.log('id: ', d.id);
					return d.id;
				},
				'dx':80,
				'dy':0,
				'font-size':10,
				'fill':'#111'})
			.append('textPath')
			.attr('xlink:href',function(d,i) {return '#edgepath'+i})
			.style("pointer-events", "none")
			.text(function(d,i){return 'label '+i});


		svg.append('defs').append('marker')
			.attr({'id':'arrowhead',
				'viewBox':'-0 -5 10 10',
				'refX':25,
				'refY':0,
				//'markerUnits':'strokeWidth',
				'orient':'auto',
				'markerWidth':10,
				'markerHeight':10,
				'xoverflow':'visible'})
			.append('svg:path')
			.attr('d', 'M 0,-5 L 10 ,0 L 0,5')
			.attr('fill', '#ccc')
			.attr('stroke','#ccc');
     

		var node = svg
			.selectAll("g.node")
			.data(nodes);

		function getNodeText(d) {
			if (d) {
				var node = _data[d.id];
				var str = d.id;
				if (_labelName && node.props && node.props[_labelName]) {
					str = node.props[_labelName];
				}
				return str;
			}
		}

		node.select('text').text(getNodeText);
		node.exit().remove();

		var nodeEnter = node.enter().append("g")
			.attr("class", "node")
			.call(force.drag);

		nodeEnter.append("svg:circle")
			.attr("r", 12)
			.attr("class", "nodeStrokeClass")
			.attr("fill", function(d) { return colorScale.range()[0]; });

		nodeEnter.on('click', this._showViewer);

		nodeEnter.append("svg:text")
			.attr("class", "textClass")
			.attr("x", 14)
			.attr("y", ".31em")
			.text(getNodeText);

		force.on("tick", function () {

			link.attr("x1", function (d) {
				return d.source.x;
			})
			.attr("y1", function (d) {
				return d.source.y;
			})
			.attr("x2", function (d) {
				return d.target.x;
			})
			.attr("y2", function (d) {
				return d.target.y;
			});

			node.attr("transform", function (d) {
				return "translate(" + d.x + "," + d.y + ")";
			});

			linkpath.attr('d', function(d) { 
				var path='M '+d.source.x+' '+d.source.y+' L '+ d.target.x +' '+d.target.y;
				return path
			});       

	        edgelabels.attr('transform',function(d,i){
	            if (d.target.x < d.source.x){
					var bbox = this.getBBox();
					var rx = bbox.x+bbox.width/2;
					var ry = bbox.y+bbox.height/2;
					return 'rotate(180 '+rx+' '+ry+')';
				} else {
	                return 'rotate(0)';
				}
	        });
		});

		// Restart the force layout.
		force
			.size([width, height])
			.charge(-800)
			.gravity(0.2)
			.linkStrength(0.2)
			.linkDistance(60)
			.start();

	},

	_onChange: function () {
		this._renderGraph(GraphStore.getNodes(), GraphStore.getEdges());
		this._setViewer(null);
	},

	_changeLabel: function (e) {
		_labelName = e.target.value;
		this._updateLayout();
	},

	_changeEdgeLabel: function (e) {
		edgeLabel = e.target.value;
		this._updateLayout();
	},

	_showViewer: function (e, type) {
		this.setState({
			activeTab: constant.propertyViewer
		});
		if (e.source) {
			this._setViewer(e.id, 'edges');
		} else {
			this._setViewer(e.id, 'nodes');
			// this.setState({currNodeViewer: e.id});
		}
	},

	_setViewer: function (id, type) {
		if (id) {
			var data;
			if (type == 'nodes') {
				data = GraphStore.getNodes()[id];
			} else {
				data = GraphStore.getEdges()[id];	
			}
			if (!data) {
				this.setState({
					activeTab: constant.versionHistory,
					nodeProps: null,
					nodeId: null
				});
			} else {
				var obj = {};
				if (type == 'nodes') {
					obj.id = data.id;
					obj.props = data.props;
				} else {
					obj = data;
				}
				this.setState({
					nodeId: id,
					nodeProps: obj,
					elementType: type
				});
			}
		}
	},

	_loading: function (isLoading) {
		this.setState({loading: isLoading});
	}

});