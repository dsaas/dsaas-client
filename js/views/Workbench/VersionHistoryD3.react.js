var React = require('react'),
	d3 = require('d3'),
	DataStructureStore = require('../../stores/DataStructureStore'),
	DataStructureActions = require('../../actions/DataStructureActions'),
	Router = require('react-router');

var target, svg, graph = {nodes:{}, edges: {}};

var width = 640,
    height = 480;

var currentNode = 0,
	defaultColour = '#eee',
	maximized = false,
	highlightColour = '#FFAB91';

var namespace, id, remove = true;

var cyReady = false, dataReady = false;

function applyStyle(element, map) {
	for (var prop in map) {
		element.style[prop] = map[prop];
	}
}


module.exports = React.createClass({
	
	componentWillMount: function () {

		cyReady = false;
		dataReady = false;
		remove = true;

		namespace = this.props.params.namespace;
		id = this.props.params.id;
		if (!namespace && !id) throw new Error('VersionHistory mounted without a data structure.');

		DataStructureActions.getVersionHistory(namespace, id);
		DataStructureStore.onChange(this._onChange);
	},

	componentDidMount: function () {
		svg = d3.select('body').append('svg')
			.attr('width', width)
			.attr('height', height);

		this._drawGraph(DataStructureStore.getVersionHistory());
	},

	componentWillUnmount: function () {
		if (remove) {
			DataStructureStore.removeChangeListener(this._onChange);	
			DataStructureStore.remove();
		} 
	},

	render: function() {
		return (
			<div className='version-history'>
				<h2>Version History</h2>
				<div id="cy" className='version-history-view'></div>
				<button id='resize-button-cy' onClick={this._resize}>[ ]</button>
			</div>
		);
	},

	_onChange: function (a, b) {
		this._drawGraph(DataStructureStore.getVersionHistory());
	},

	_drawGraph: function (data) {
		if (svg && data) {
		
			var label = 0;
			for (let parent in data) {
				let children = data[parent];

				if (!graph.nodes[parent])
					graph.nodes[parent] = {label: label++, value: -1, x: width/2, y: height/2};

				if (children) {
					for (let c in children) {
						let child = children[c];
						if (!graph.nodes[child])
							graph.nodes[child] = {label: label++, value: -1, x: width/2, y: height/2};

						var edge = parent + ',' + child;
						if (!graph.edges[edge])
							graph.edges[edge] = {source: graph.nodes[parent], target: graph.nodes[child]};
					}
				}
			}

			let nodes = [];
			let position = 0;
			for (let key in graph.nodes) {
				nodes.push(graph.nodes[key]);
				graph.nodes[key].value = position++;
			}

			let links = [];
			for (let key in graph.edges) {
				let edge = graph.edges[key];
				var graphEdge = {};
				graphEdge.source = edge.source.value;
				graphEdge.target = edge.target.value;
				links.push(graphEdge);
			}


			// var nodes = [
			//     { x:   width/3, y: height/2 },
			//     { x: 2*width/3, y: height/2 }
			// ];

			// var links = [
			//     { source: 0, target: 1 }
			// ];

			console.log(nodes, links);
			var force = d3.layout.force()
			    .size([width, height])
			    .nodes(nodes)
			    .links(links);


			force.linkDistance(width/2);


			var link = svg.selectAll('.link')
			    .data(links)
			    .enter().append('line')
			    .attr('class', 'link');

			// Now it's the nodes turn. Each node is drawn as a circle.

			var node = svg.selectAll('.node')
			    .data(nodes)
			    .enter().append('circle')
			    .attr('class', 'node');


			force.on('end', function() {

			    node.attr('r', width/25)
			        .attr('cx', function(d) { return d.x; })
			        .attr('cy', function(d) { return d.y; });

			    link.attr('x1', function(d) { return d.source.x; })
			        .attr('y1', function(d) { return d.source.y; })
			        .attr('x2', function(d) { return d.target.x; })
			        .attr('y2', function(d) { return d.target.y; });

			});

			force.start();

		}
	},

	_updateVersion: function (e) {
		target = e.cyTarget;
		cy.center(e.cyTarget);
		cy.getElementById(currentNode).style('background-color', defaultColour);
		e.cyTarget.style('background-color', highlightColour);
		currentNode = e.cyTarget.id();
		this.props.changeVersion(e.cyTarget.id());
	},

	_resize: function (e)  {
		var cyContainer = document.getElementById('cy');
		var resizeButtonCy = document.getElementById('resize-button-cy');
		if (!maximized) {
			applyStyle(cyContainer, {position: 'absolute', top: 0, left: 0, zIndex: 1000, width: '100%', height: '100%'});
			applyStyle(resizeButtonCy, {position: 'absolute', bottom: '10px', left: '10px', zIndex: 1001});
		} else {
			applyStyle(cyContainer, {position: 'initial', zIndex: 0, width: '400px', height: '400px'});
			applyStyle(resizeButtonCy, {position: 'initial', zIndex: 0, bottom:0, left: 0});
		}

		maximized = !maximized;
		cy.resize();
		console.log(target);
		if (target) cy.center(target);
	},

	statics: {
		setRemove: function (r) {
			remove = r;
		}
	}

});